from atelier.invlib import setup_from_tasks
# Currently not maintained.
ns = setup_from_tasks(
    globals(),
    use_dirhtml=True,
    doc_trees=[],
    # tolerate_sphinx_warnings=True,
    # blogref_url="http://hw.lino-framework.org",
    revision_control_system='git',
    selectable_languages=('et','en', 'ru'),
    cleanable_files=[
        'docs/rss_entry_fragments/*'])
    # intersphinx_urls=dict(docs="https://sinod.laudate.ee"))
