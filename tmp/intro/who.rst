====================
Who can participate?
====================

Short answer: Everybody who wants to help the Church on her synodal journey of
seeking what is good and true. Actually the mere fact of participating in a
consultation meeting shows your good will. If you feel angry with the Church,
you are actually especially welcome.

The work of evangelization and the message of salvation would not be
comprehensible without Jesus’ constant openness to the widest possible audience,
which the Gospels refer to as the *crowd*, that is, all the people who follow
him along the path, and at times even pursue him in the hope of a sign and a
word of salvation (...). The proclamation of the Gospel is not addressed only to
an enlightened or chosen few. (:term:`PD`, 18)


While **all the baptized** are specifically called to take part in the Synodal
Process, **no one** – no matter their religious affiliation – **should be
excluded** from sharing their perspective and experiences, insofar as they want
to help the Church on her synodal journey of seeking what is good and true. This
is especially true of those who are most vulnerable or marginalized.
(:term:`Vademecum`, section 2.1)


.. inactive:

  (Source: :term:`Vademecum`, section 2.1)

  We see throughout the Gospels how Jesus reaches out to all. He does not only
  save people individually but as a people that he gathers together, as the one
  Shepherd of the entire flock (cf. John 10:16). The ministry of Jesus shows us
  that no one is excluded from God’s plan of salvation.

  The work of evangelization and the message of salvation cannot be understood
  without Jesus’ constant openness to the widest possible audience. The Gospels
  refer to this as the crowd, composed of all the people who follow Jesus along
  the path and everyone that Jesus calls to follow him. The Second Vatican Council
  highlights that “all human beings are called to the new people of God” (LG, 13).
  God is truly at work in the entire people that he has gathered together. This is
  why “the entire body of the faithful, anointed as they are by the Holy One,
  cannot err in matters of belief. They manifest this special property by means of
  the whole people's supernatural discernment in matters of faith when from the
  Bishops down to the last of the lay faithful, they show universal agreement in
  matters of faith and morals” (LG, 12). The Council further points out that such
  discernment is animated by the Holy Spirit and proceeds through dialogue among
  all peoples, reading the signs of the times in faithfulness to the teachings of
  the Church.

  In this light, the objective of this diocesan phase is to consult the People of
  God so that the Synodal Process is carried out through listening to **all the
  baptized**. By convoking this Synod, Pope Francis is inviting all the baptised
  to participate in this Synodal Process that begins at the diocesan level.
  Dioceses are called to keep in mind that the main subjects of this synodal
  experience are all the baptised. Special care should be taken to involve those
  persons who may risk being excluded: women, the handicapped, refugees, migrants,
  the elderly, people who live in poverty, Catholics who rarely or never practice
  their faith, etc. Creative means should also be found in order to involve
  children and youth.

  Together, all the baptised are the subject of the *sensus fidelium*, the living
  voice of the People of God. At the same time, in order to participate fully in
  the act of discerning, it is important for the baptised to hear the voices of
  other people in their local context, including people who have left the practice
  of the faith, people of other faith traditions, people of no religious belief,
  etc. For as the Council declares: “The joys and the hopes, the griefs and the
  anxieties of the men of this age, especially those who are poor or in any way
  afflicted, these are the joys and the hopes, the griefs and the anxieties of the
  followers of Christ. Indeed, nothing genuinely human fails to raise an echo in
  their hearts” (:term:`GS`, 1).
