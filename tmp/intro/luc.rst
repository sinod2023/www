========================
Call for participation
========================

The world seems to be quite stormy these times: pandemic, climate change, fake
news and information pollution, terrorism, ...

Also the Catholic Church is experiencing storms. Loud debates between
conservative and progressive schools, or scandals about sexual or financial
topics, etc.

This kind of storms is nothing new in the long history of Church. Such storms
occur every now and then, and the Church has survived them all.

A faithless historian once worked for several months in the Vatican's archives
on some research project.  After this he became a faithful and asked for being
baptized. His friends asked "why?". He answered: "In the archives of the Vatican
I saw how many mistakes have been done by the Church during its history. If a
corporation would make this kind of mistakes, it would soon go bust (bankrupt).
Why is the Church still there? There must be some divine will behind it." (I
didn't fact-check that story, but *se non è vero, è ben trovato*.)

Yes, the Church survives all storms and crises, it actually grows and evolves
*through* them. But *how exactly* does this work? It has always worked somehow
automatically or "because God wants it", as we use to say. The Vatican has of
course all kind of administrative documents and rules, but *why these rules are
as they are*?

How does the Church function?
The *modus vivendi et operando* of the Church is obviously neither democratic nor monarchistic.
A comprehensive answer to this question is being demanded with more and more urgency.
Not only because internal debates demand clarifications.
Not only because public order demands explanations.

There is another, even more important reason:
humankind as a whole struggles with exactly the same kind of challenges as the Church.
The Church obviously has a know-how about living together, and this know-how
might be useful for public administrations and governments as well.

The Church herself started only quite recently to discover that this model has
something special and that it deserves a name.  The word "synodality", actually
a neologism, emerged as a name for this model.
Synodality is our way of living together, sustainably and united in diversity.

The Church has done already some work for explaining this synodality.
A number of documents have been published by the Vatican.
See the "Resources" section on our website.

.. The first explicit document is
  `Synodality in the life and mission of the Church.
  <https://www.vatican.va/roman_curia/congregations/cfaith/cti_documents/rc_cti_20180302_sinodalita_en.html>`__,
  a study lead by the :term:`ITC` and published in March 2018).

But even more work is ahead. "The harvest is plentiful, but the labourers are
few. Therefore pray earnestly to the Lord of the harvest to send out labourers
into his harvest." (`Lk 10:2 <https://www.bibleserver.com/ESV/Luke10%3A2>`__)

In order to prepare the XVI. synod of bishops, Francis started a huge and
unprecedented project: he wants to hear the voice of everybody.  Not only
Catholics, but all the Baptized and even non-Christians are invited to
participate.


.. [Jah, paavst Franciscus ei karda utoopilisi ideid sõnastada.  Noh mina olen küll
  juba paar korda elus mõelnud, et tahaksin paavstile kirja kirjutada ja talle
  mõned ideed jagada. Sina mitte? Ma lihtsalt pole siiani jõudnud seda teha. Ja
  kas ta ikka minu kirjakest päriselt loeks, on küsitav. Aga nüüd on siis selleks
  võimalus.]

Believe it or not: on this journey we *are* together with the *whole* mankind.
Synodality is unavoidable.
That's why I invite you cordially to participate in the Synod and to submit your response to our consultation.
Maybe *your* voice is the straw to break the camel's back that makes things
change (or prevents things from changing)

Luc Saffre, Tallinn, October 2021
