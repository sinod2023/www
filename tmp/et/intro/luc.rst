===============================
Üleskutse osalemiseks
===============================

Maailm tundub olevat viimasel ajal üsna tormiline: pandeemia, kliimakriis,
inforeostus, radikalism, ...

Ka Katoliku Kiriku sees toimuvad tormid, nt. "konservatiivse" ja "progressiivse"
veendumuste vahel, või Kiriku võimustruktuurid satuvad avaliku kriitika alla
seksuaalses või finantsilistes skandaalites, jne...

Sellised tormid ei ole uus nähtus Kiriku pikas ajaloos, need on alati olnud ja
Kirik elab need üle.

Üks mitteusklik ajaloolane töötas mitu kuud Vatikani arhiivides mõne uurimistöö
jaoks. Pärast seda hakkas ta uskuma ja lasi ennast ristida. Sõbrad küsisid
"Miks?". Ta vastas: Ma nägin arhiivides, millised hullud vead Kirik on teinud
ajaloos. Kui mõni riik või ettevõte teeks selliseid vigu nagu Kirik on teinud,
oleks see ammu pankrotis. Miks Kirik pole ammu kadunud? Seal taga peab mingi
jumalik tahtejõud olema. (Ma pole kindel, kas see on pärislugu, aga *se non è
vero, è ben trovato*.)

Jah, Kirik jääb ellu hoolimata tormidest ja kriisidest, nende kaudu ta isegi
kasvab ja areneb. Aga *kuidas* see toimib täpsemalt? See on alati toimunud
kuidagi automaatselt, kuidagi "sellepärast, et Jumal tahab", nagu õeldakse.
Kiriku institutsioonil on muidugi igasugused juriidilised dokumendid, aga *miks
need reeglid on nii, nagu nad on*?

Kuidas Kirik toimib?
Kiriku struktuur on ilmselt ei demokraatlik ega monarhistlik.
Sellele küsimusele nõutakse tänapäeval üha tungivamalt koherentse vastuse.
Mitte ainult sellepärast, et avalik kord nõuab seletusi. See ka.
Mitte ainult sellepärast, et Kiriku sees käivad arutelud. See ka.
Aga on olemas üks palju olulisem põhjus: kogu inimkond on tegelikult hädas nendesamade
väljakutsedega, mis Kirikul on.
Kiriku *modus vivendi et operando* on mudel, millest avalik ühiskond saaks võib-olla midagi õppida.

Kirik ise hakkab alles tasapisi avastama, et sellel mudelil on midagi erilist.

Mõned asjad on Vatikan juba sõnadesse pandud (loe :doc:`/guide/index`). Palju
töö on tehtud, aga veel rohkem jääb teha, ka siin Eestis. *Lõikust on palju,
töötegijaid aga vähe. Paluge  siis lõikuse Issandat, et ta saadaks töötegijaid
välja oma lõikusele!* (Lk 10:2)

XVI piiskopisinodi ettevalmistamiseks alustas paavst Franciscus hiigelsuure ja
enneolematu projekti:  ta tahaks kuulda *iga inimese* hääl sellel teemal.  Mitte
ainult katoliiklased, vaid kõik kristlased ja mitteristitud inimesed on kutsutud
vastama.

.. [Jah, paavst Franciscus ei karda utoopilisi ideid sõnastada.  Noh mina olen küll
  juba paar korda elus mõelnud, et tahaksin paavstile kirja kirjutada ja talle
  mõned ideed jagada. Sina mitte? Ma lihtsalt pole siiani jõudnud seda teha. Ja
  kas ta ikka minu kirjakest päriselt loeks, on küsitav. Aga nüüd on siis selleks
  võimalus.]

Usu seda või mitte: me *oleme* sellel teel koos kogu inimkonnaga. Paratamatult.
Võib-olla on just Sind vaja! 

Luc Saffre, Tallinn, oktoobris 2021
