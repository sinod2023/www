===================================
Mis tähendab "sinodaalne?"
===================================

Konsultatsiooni küsimus algab tähelepanekuga:  *Sinodaalne Kirik on
Evangeeliumit kuulutades "koos teel".* Okei, aga mida tähendab "Kirik on
sinodaalne"? Ju kindlasti mitte, et Kirik toimub ainult piiskoppide
koosolekutel?!

Me teame, mis on "sinod". See on ju mingi kiriklik koosolek, eks? Tegelikult on
sõna **sinod** nii laia tähendusega, et sellest võiks doktoritöö kirjutada. See
tuleb kreeka keelest *συν* (koos) ja *ὁδός* (teekond). Katoliku Kirik hakkas
seda sõna kasutatama (II Vatikani Kirikukogu tulemusena) alles aastal 1983, kus
defineeriti `piiskoppide sinod
<https://en.wikipedia.org/wiki/Synod_of_Bishops_in_the_Catholic_Church>`__.

  "What the Lord is asking of us is already in some sense present in the very
  word "Synod." Journeying together – laity, pastors, the Bishop of Rome – is an
  easy concept to put into words, but not so easy to put into practice."
  -- Pope Francis

Piiskoppide sinode on siiani olnud viisteist ja see, millest me siin räägime, on
kuueteistkümnes ja see toimub oktoobris 2023.  Sinoditel arutlevad "sinodiisad"
tähtsaid asju ja sealt võivad (aga ei pea) algata suured muutused.


.. TODO: Eestikeelne Vikipeedia artikel vajaks töötlemis. https://et.wikipedia.org/wiki/Sinod

  https://en.wikipedia.org/wiki/Synod_of_Bishops_in_the_Catholic_Church#Assemblies

Väljendit "sinodaalne tee" kasutas Franciscus esimest korda oma kõnes
:term:`20151017`. Võib õelda, et see on üks Franciscuse pontifikaadi
iseloomustav võtmesõna. Sellest vaimustatuna alustasid Saksamaa katoliiklased
2019. aastal samanimelise projekti. Nad veidi pettusid, kui paavst neile mingil
hetkel ütles "Rahunege maha, need teemasid tuleb *kogu* Kirikuga koos läbi
rääkida, pole mõtet kiirustada." Võib õelda, et :term:`Sinodaalsuse Sinod` on
muu hulgas Vatikani vastukaja sakslaste algatusele.

.. https://www.vaticannews.va/de/kirche/news/2021-10/deutschland-synodaler-weg-endet-vorzeitig-mangelnder-teilnehmer.html
