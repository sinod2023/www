==================
Kuidas kaasa lüüa?
==================

- Palveta meiega Kiriku ja Sinodi eest.

- Osale mõnel konsultatsiooni koosolekul.

- Liitu meie :doc:`uudiskirjaga </news>`.

- Korralda mõnda konsultatsiooni koosolekut.

- Meil on veel vaja **tõlkijat** (inglise ja eesti keelest vene keelde). Vaata :doc:`/teams/editors`.

- Meil on veel vaja **disainerit**, kes teeb plakateid, voldikuid ja muu asju paberil.

- Osale :doc:`sinoditiimis </teams/core>` (mõtle ja räägi kaasa projekti üldkorraldamisel)


.. toctree::
    :hidden:

    editors
    pastor
    core
