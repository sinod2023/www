=============
Kiriku jargon
=============


.. glossary::

  Sinodaalsuse Sinod

    XVI. Regulaarne Sinodipiiskoppide Üldkoosolek, mis toimub Roomas oktoobris
    2023 ja mille teemaks on "Sinodaalse Kiriku eest: Osadus, Kaasamine,
    Kuulutamine"

    .. The XVI. Ordinary General Assembly of the Synod of Bishops, to be held in
      October 2023 on the theme "For a Synodal Church: Communion, Participation,
      and Mission."

    Sinodi ettevalmistus algab juba kaks aastat varem *konsultatsioonifaasiga*,
    mille jooksul korjatakse sisendmaterjal.
    Sindole järgneb *implementatsioonifaas*, mis jälle kaasab kohalikud tiimid.

    .. The synod starts in October 2021 with a "diocesan phase", during which local
      teams will collect input. It will be followed by the implementation phase
      that will again involve the local teams.

    Ametlik Sinodi veebisait: https://www.synod.va
