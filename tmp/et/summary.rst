=======
Süntees
=======

Siin tekib see aruanne, mida me Eesti Apostelliku Administratuuri poolt
kavatseme anda Sinodile.

.. contents::
  :depth: 1
  :local:

Introduction
============

Our hope for this report is that it may give a *valuable* and *true* input to
the Synod on Synodality.

This report is published as a static website with a printable summary. We expect
the bishops who attend the synod to read at least this printable summary. Other
parts of the website are published in the hope of giving useful background
information to those who which to know more.

We gave our best to focus on maximum inclusion and participation among baptized
Catholics.

Limitations
===========

There are about 4500 Catholics living in Estonia. We are a relatively small
group within the group of Christians in Estonia (300.000), which is itself a
very small group within the population of Estonia (1.330.000), which is itself a
tiny group of the world population (0.0168%).  Recommended readings: `Religion
in Estonia <https://en.wikipedia.org/wiki/Religion_in_Estonia>`__ So please
don't expect this report to produce statistically representative arguments for
the future of the Church.

.. https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population

This report has been compiled by a volunteer who is not even Estonian.
So please don't expect this report to be authoritative or relevant at a
sociological, theological or political level.
