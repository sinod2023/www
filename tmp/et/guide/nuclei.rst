========================
Kümme temaatilist tuuma
========================

Oma kogemuse kajastamiseks mõtle näiteks järgmistele aspektidele, mille kaudu
Kirik elab oma sinodaalset elu. 

.. Kogemuste esiletoomiseks ja konsultatsioonile rohkem kaasa aitamiseks toome
  allpool välja kümme temaatilist tuuma, mis väljendavad „elatud sinodaalsuse”
  erinevaid tahke.

**1. TEEKAASLASED**

*Kirikus ja ühiskonnas oleme samal teel kõrvuti.*

Kes on teie kohalikus kirikus need, kes on “ühisel teekonnal”? Kui me ütleme:
„meie kirik”, siis kes on selle osa? Kes palub meil koos teel olla? Kes on
teekaaslased, sealhulgas väljaspool kirikuala? Millised isikud või rühmad jäävad
äärealadele, nimelt või tegelikult?

**2. KUULAMINE**

*Kuulamine on esimene samm, kuid see nõuab avatud meelt ja südant, ilma eelarvamusteta.*

Keda meie konkreetne kirik „peab kuulama”? Kuidas kuulatakse ilmikuid, eriti
noori ja naisi? Kuidas me integreerime pühitsetud meeste ja naiste panust?
Milline koht on vähemuste, kõrvalejäetute ja tõrjutute häälel? Kas tuvastame
eelarvamused ja stereotüübid, mis takistavad meid kuulamast? Kuidas me kuulame
sotsiaalset ja kultuurilist konteksti, milles me elame?

**3. JULGELT RÄÄKIMINE**

*Kõik on kutsutud julgelt ja kammitsematult rääkima, see tähendab vabadust,
tõde ja armastustt integreerides.*

Kuidas edendame kogukonnas ja selle organisatsioonides vaba ja autentset
suhtlusstiili, ilma kahepalgelisuse ja oportunismita? Ja seoses ühiskonnaga,
mille osa me oleme? Millal ja kuidas me suudame öelda, mis on meie jaoks
oluline? Kuidas toimib suhe meediasüsteemiga (mitte ainult katoliku meediaga)?
Kes räägivad kristliku kogukonna nimel ja kuidas neid valitakse?

**4. PÜHITSEMINE**

*“Ühine teekond” on võimalik ainult siis, kui see põhineb Sõna ühisel kuulamisel ja Armulaua pühitsemisel.*

Kuidas palve ja liturgiline pühitsemine inspireerivad ja suunavad meie „ühist
teekonda”? Kuidas nad inspireerivad kõige olulisemaid otsuseid?  Kuidas edendada
kõigi ustavate aktiivset osalemist liturgias ja pühitsusfunktsiooni täitmisel?
Millist ruumi antakse lugeja ja akolüüdi teenistustele?

**5. KAASVASTUTAMINE MISSIOONIS**

*Sinodaalsus teenib Kiriku missiooni, milles kõik tema liikmed on kutsutud osalema.*

Kuna me kõik oleme misjonijüngrid, siis kuidas kutsutakse iga ristitud inimest
missiooni peategelaseks? Kuidas toetab kogukond oma liikmeid, kes on pühendunud
ühiskonnas teenimisele (sotsiaalne ja poliitiline pühendumine, teadusuuringud ja
õpetamine, sotsiaalse õigluse edendamine, inimõiguste kaitse ja ühise kodu eest
hoolitsemine jne)? Kuidas aitate neil missiooni loogikas neid kohustusi täita?
Kuidas tehakse otsuseid missiooniga seotud valikute kohta ja kes selles osaleb?
Kuidas on integreeritud ja kohandatud erinevad traditsioonid, mis moodustavad
paljude kirikute, eriti idamaade, pärandi, seoses sinodaalse laadiga, silmas
pidades tõhusat kristlikku tunnistust? Kuidas toimib koostöö piirkondades, kus
on erinevad sui iuris kirikud?

**6. DIALOOG KIRIKUS JA ÜHISKONNAS**

*Dialoog on püsivuse tee, mis hõlmab ka vaikust ja kannatusi, kuid mis on
võimeline koguma inimeste ja rahvaste kogemusi.*

Millised on dialoogi kohad ja viisid meie kirikus? Kuidas käsitletakse
lahknevusi visioonides, konflikte ja raskusi? Kuidas edendada koostööd
naaberpiiskopkondadega, piirkonna usukogukondadega ja nende vahel, ilmalike
ühenduste ja liikumistega ja nende vahel jne? Millised kogemused dialoogist ja
jagatud pühendumusest on meil teiste religioonide usklike ja mitteusklikega?
Kuidas peab kirik dialoogi ja õpib teistest ühiskonna sektoritest: poliitika,
majandus, kultuur, kodanikuühiskond ja vaesed…?

**7. TEISTE KRISTLIKE KONFESSIOONIDEGA**

*Dialoogil erinevate kristlike konfessioonidega, mida ühendab üks ristimine, on
sinodaalsel teekonnal eriline koht.*

Millised suhted on meil teiste kristlike konfessioonide vendade ja õdedega?
Milliseid valdkondi need puudutavad? Milliseid vilju oleme sellest „ühisest
teekonnast” saanud? Millised on raskused?

**8. AUTORITEET JA OSALEMINE**

*Sinodaalne kirik on kaasav ja kaasvastutav kirik.*

Kuidas me tuvastame taotletavad eesmärgid, nende saavutamise viisi ja vajalikud
sammud? Millised on meeskonnatöö ja kaasvastutamise tavad? Kuidas edendatakse
ilmikute teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad sinodaalsed
organid konkreetse kiriku tasandil? Kas need on viljakad kogemused?

.. Helve:
  Kuidas määrame taotletavad eesmärgid, nende saavutamise viisid ja astutavad
  sammud? Kuidas toimub valitsemine konkreetselt meie Kirikus? Millised on
  koostöö ja kaasvastutamise tavad? Kuidas soodustatakse ilmikute
  teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad sinodaalsed
  kogud konkreetse Kiriku tasandil? Kas need on tulemuslikud kogemused?


**9. ÄRATUNDMINE JA OTSUSTAMINE**

*Sinodaalses laadis tehakse otsuseid arusaamise abil, mis põhineb konsensusel,
mis lähtub ühisest kuulekusest Vaimule.*

Milliste protseduuride ja meetoditega me koos äratundmisele jõuame ja otsuseid
teeme? Kuidas saab neid parandada? Kuidas edendame otsuste tegemisel osalemist
hierarhiliselt struktureeritud kogukondades? Kuidas me liigendame konsultatiivse
etapi aruteluga, otsuse tegemise protsessi otsuse vastuvõtmise hetkega? Kuidas
ja milliste vahenditega edendame läbipaistvust ja aruandlust?

.. Helve:

  - eristamine ja otsustamine -- Sinodaalselt toimides tehakse otsuseid eristamise
    kaudu, tuginedes konsensusele, mis lähtub ühisest kuulekusest Vaimule.

    Milliste protseduuride ja meetoditega eristame koos ja teeme otsuseid? Kuidas
    saaks neid täiendada? Kuidas me soodustame otsustamises osalemist
    hierarhiliselt korraldatud kogukondades? Kuidas me ühendame omavahel
    konsultatiivse etapi ja kaalutlemisetapi, otsuse tegemise protsessi ja otsuse
    tegemise hetke? Kuidas ja milliste vahenditega soodustame läbipaistvust ja
    vastutavust?



**10.\  KUJUNEMINE SINODAALSUSES**

*Ühise teekonna vaimsusest on kutsutud kujunema kasvatuslik põhimõte inimese ja
kristlase, perekondade ja kogukondade kujunemisel.*

Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
vastutusrollid, et muuta nad võimekamaks „ühisel teekonnal”, üksteist kuulates
ja dialoogi pidades? Millist moodust me pakume äratundmise ja autoriteedi
rakendamiseks? Millised vahendid aitavad meil mõista meid ümbritseva kultuuri
dünaamikat ja selle mõju meie kirikustiilile?

.. Helve:
  - täienda ja kasvata -- koos rändamise vaimsus on kutsutud saama inimisiku ja kristlase, perekondade ja kogukondade kujundamise kasvatuslikuks põhimõtteks.

    Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
    vastutav roll, et nad oleksid veelgi suutelisemad "koos rändama", üksteist
    kuulates ja dialoogi pidades? Millist kujundamist pakume eristamise
    hõlbustamiseks ja sinodaalselt valitsemiseks? Millised vahendid aitavad meil
    mõista ümbritsevas kultuuris toimivaid jõude ja nende mõju meie laadi
    Kirikule?



(Allikas: :term:`PD` punkt 30 ja :term:`Vademecum` punkt 5.3)
