=======
Küsimus
=======

Armsad vennad ja õed! Kirik vajab teie kaasamõtlemist, kogemusi ja arvamusi.
Ajavahemikus oktoober 2021 – märts 2022 kutsume teid andma tagasisidet teemal:

  Sinodaalne Kirik on Evangeeliumit kuulutades "koos teel". Kuidas see
  "koos teel olemine" täna sinu kohalikus Kirikus kulgeb? Milliseid samme
  kutsub Vaim meid astuma, et me oma "koos teel olemises" kasvaksime?

  .. või: Sinodaalne Kirik, et kuulutada Evangeeliumit, on koos teel.

Vastamiseks palume teil:

- küsida endalt, milliseid kogemusi teie konkreetses kirikus põhiküsimus meelde
  tuletab;

- lugeda neid kogemusi sügavamalt uuesti: milliseid rõõme nad tekitasid?
  Milliste raskuste ja takistustega nad on kokku puutunud? Milliseid haavu on
  nad päevavalgele toonud? Milliseid teadmisi nad on tekitanud?

- koguda vilju, et neid jagada: Kus nendes kogemustes Vaimu hääl kostab? Mida ta
  meilt küsib? Millised punktid tuleb kinnitada, millised on muutuse
  väljavaated, milliseid samme tuleb astuda? Kus me konsensuse registreerime?
  Millised teed avanevad meie konkreetse kiriku jaoks?


(Allikas: :term:`PD` 26)
