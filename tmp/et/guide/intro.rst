============
Sissejuhatus
============

Tuleva sinodi teemaks on "For a synodal Church: Communion, Participation and
Mission".

By convening the :term:`Sinodaalsuse sinod`, Pope Francis invites the entire
Church to reflect on a theme that is decisive for its life and mission: "It is
precisely this path of synodality which God expects of the Church of the third
millennium." (:term:`Vademecum` 1.2)

Sinodi ettevalmistuse esimene avalik faas on **konsultatsioon**. Sellel faasil
kuulame, mida Eesti rahvas vastab Paavsti küsimusele.

Olulisem veel kui tegelik vastuste korjamine, selle faasi eesmärk on pakkuda
võimalikult paljudele inimestele tõesti sinodaalse kogemuse: kuulata üksteist ja
minna koos edasi Püha Vaimu juhendamisel.
