===================
Kirik on sinodaalne
===================

Kiriku kooselamise viis ei ole demokraatlik ega monarhistlik ega anarhistlik.
Paistab, et kõige parem sõna, mis seda kirjeldab, on "sinodaalne".
**Sinodaalsus** on Katoliku Kiriku iseloomulik kooselamise viis (*modus vivendi et operando*).
Kirik ise hakkab sellest alles tasapisi aru saama.

  "Synodality is the way of being the Church today according to the will of God,
  in a dynamic of discerning and listening together to the voice of the Holy
  Spirit." -- Pope Francis
