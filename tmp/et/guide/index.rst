================
Teejuht Sinodile
================

.. toctree::
    :maxdepth: 1

    intro
    question
    meeting
    nuclei
    synodality
    dimensions
    principles
    ldp
    attitudes
    dispositions
    temptations
