=========
Koosolek
=========

TODO: translate from English

.. minu töö on pooleli:

  Konsultatioonifaasis toimuvad **kohtumised**. Kohtumisi on erinevat sorti:

  - **1:1 vestlus** sinu ja :doc:`koguja </teams/collector>` vahel
  - **Külaskäik** -- mõni kollektiiv kutsub koguja enda juurde ja osaleb ühiselt
  - **Avalik koosolek** korraldatud mõne koguduse poolt

  Iga kohtumine koosneb järgnevatest osadest:

  - Sissejuhatus (vajadusel lugeda :doc:`/intro/index`)
  - Palve (koos lugeda :doc:`/adsumus`)
  - Küsimuse esitamine (:doc:`question`)
  - Meditatioon: jäta inimestele aega seedimiseks
  - Jagamine: anda aega neile, kes soovivad vastata teiste ees
  - Korjamine: paneme kirja osalejate vastuseid
  - Avalikustamine: veebiredaktor lisab vastuseid veebisaidile.



  (Allikad: :term:`PD` lk.26, :term:`Vademecum` Appendix B)
