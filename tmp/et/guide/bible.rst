==================
Piibellikud alused
==================

(Inspireeritud artiklist `Synodalität als Stärkung der Kirche
<https://www.die-tagespost.de/kirche-aktuell/weltkirche/synodalitaet-als-staerkung-der-kirche-art-221393>`__)

(Mt 6:9) Meieisa palve alustab sõnaga "meie", mitte "minu". Me oleme sellel
teel *koos*. Usk ei ole võimalik ilma teiste inimeseta. Minu kaasinimene ei ole
takistus või vaenlane, vaid vastupidi: Issand saadab teda minule abiks.

(Mk 3:13) Jüngrite valimisel ülatab valitute mitmekesisus. Neil on väga
erinevad politilisi ja kultuurilisi taustasid ja hoiakuid. Selline kirju trupp
poleks iialgi kokku tulnud, kui Jeesu poleks neid kutsunud.

(Mt 4:17) Pöörduge! on üks iseloomustav osa Jeesuse jutlustes. Sellepärast nõuab
ka iga sinodaalne protsess meie valmisolek ümbermõtlemiseks,
ümbersihisstamiseks, pöördumiseks.

(Jh 3:3) Uuesti sündimine. Kristlus pole kunagi olnud normaalne, loomulik ja
enestmõistav. See on alati olnud uus, üleloomulik ja eriline.

(Jh 14:6) "Mina olen tee, tõde ja elu" -- Kristus on kristliku elu eeskuju ja
mõõdupulk. Ta on ainus tee Isa juurde. tema õpetus on jumaliku volitusega antud
ja ületab iga inimese mõistuse (Mk 1:22) ja peaks sellepärast olema iga
koosoleku lähtepunkt: kuidas Jeesus hindaks, mõstaks kohut, otsustaks ja
tegutseks? Kuidas meie täidame tema käsku?

(1 Jh 4:1) "ärge usaldage iga vaimu, vaid katsuge vaimud läbi, kas
nad on Jumalast"

(Mt 6:10) "Sinu tahtmine sündigu". Sinodaalse protsessi eesmärk on siiras otsing
Jumala tahtmise järgi, mitte omaenda plaane ja veendumuste läbisurumine või
neile enamuse kasvatamine. Keegi ei saa väita, et ta teaks Juamal tahtmist mõnes
konkreetses arutelus. Aga kui sinodaalne koosolek siiralt otsib, innukalt
palvetab, avatult suhtleb, siis on lootus, et see kuulab Jumala käsku. Oluline
tunnusmärk on see,
