===========
Resurssid
===========

.. glossary::

  PD

    Preparatory Document

    http://www.synod.va/en/documents/versione-desktop-del-documento-preparatorio.html

  Vademecum

    A 22-page handbook for the diocesan phase of the synod published by the Vatican in September 2021.

    http://www.synod.va/en/documents/vademecum.html

  20151017

    Franciscus, `Address at the Ceremony Commemorating the 50 th Anniversary
    of the Institution of the Synod of Bishops
    <https://www.vatican.va/content/francesco/en/speeches/2015/october/documents/papa-francesco_20151017_50-anniversario-sinodo.html>`__
    (17 oktoboer 2015).
