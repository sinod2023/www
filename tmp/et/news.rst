=======
Uudised
=======

Kõige lihtsam viis olla meiega ühenduses on liituda meie uudiskirjaga
"Sinodiuudised".  Selle meililisti teemaks on kõik, mis toimub Eestis seoses
Sinodiga. Siiani on ilmunud järgmised uudiskirjad:

- 2021-10-02 :doc:`/blog/2021/1002`

Uudiskirjaga liitumiseks: Kui Sul on Google konto, siis `kliki siia
<https://groups.google.com/g/sinodiuudised/about>`__ ja vajuta "Join group". Kui
sa ei soovi luua Google kontot, siis saada meile lihtsalt e-meili ja palu, et
sind lisatakse listile.

Kutsu ka oma sõpru liituda selle listiga. Jaga neile lihtsalt
käesoleva lehe lingi!

Kui sul on midagi õelda kõikidele listi liikmetele, siis saada e-kirja
aadressile sinodiuudised@googlegroups.com. Pea meeles, et kõik listile saadetud
kirjad on availkult näha.
