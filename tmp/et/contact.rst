=======
Kontakt
=======

Igasugune tagasiside on teretulnud.

.. list-table:: Inimesed

  - * | Luc Saffre
      | Sinodi kontaktisik Eestis

    * | +372/56672435
      | luc.saffre@gmx.net

  - * | Marge-Marie Paas
      | meediakoordinaator

    * |
      | info@katoliku.ee

  - * | Isa Philippe Jourdan
      | Eesti Katoliiklaste piiskop

    * küsi järgi

  - * Sinoditiim on 5 vabatahtlike rühm, mis aitab kaasa mõelda.

    *
