:date: 2021-10-02

=============================
Palvetage sinodi eest!
=============================

Tere tulemast meililistis "Sinodiuudised".

Paavst Franciscus kutsub kogu Kirikut osaleda piiskoppide sinodil teemal "For a
Synodal Church: Communion, Participation, Mission" ("Sinodaalse Kiriku jaoks:
osadus, osalemine ja mission"). See hiigelprojekt on kirjeldatud kahes Vatikani
dokumentides [#f1]_.  Marge kirjutas ka `projekti tutvustuse
<https://katoliku.ee/index.php/et/uudised/vaelismaa/640-piiskoppide-sinod>`__.
Eestis toimub avapauk 17. oktoobril.

Mina (Luc) vastasin "Jah" isa Philippe'i küsimusele, kas ma oleksin nõus olla
selle projekti referent Eestis ("Diocesan Contact Person"). See on auväärne
ülesanne, mis tuli mulle üllatusena. Kuidas küll isa Philippe sellise idee peale
tuli? Küsige tema käest, mina imestan siiani... Ma hoiatasin teda, et mul puudub
pädevust selliste projektide juhendamisel ning et mul on töö ja pere kõrval
piiratud energia ja aega. Aga ma luban teha oma parimat, et seda ülesannet
täita.

Tuleva aasta aprilliks pean ma kirjutama Vatikanile, mida Eesti kristlastel on
vastata järgmisele küsimusele:

  A synodal Church, in announcing the Gospel, 'journeys together'. How is this
  'journeying together' happening today in your local Church? What steps does the
  Spirit invite us to take in order to grow in our 'journeying together?'

Ma ei pea kirjutama *omaenda* vastuse, vaid süntees sellest, mida *Eestis elavad
ristitud inimesed* vastavad. Mitte ainult vaimulikud ja ka mitte ainult need,
kes nagunii oma arvamust tänavatel välja kuulutavad.

Aga mida tähendab "sinodaalsus"? Ja miks paavst küsib selline küsimus? Ja miks
küsida "iga ristitud inimeselt"? Ja kuidas me hakkame seda korraldama, kuidas
see hakkab toimuma konkreetselt siin Eestis? Palju küsimusi!

Üks asi on juba nüüd selge: See sinod on **meie kõigi jaoks enneolematu võimalus
Kirikuna kasvada**. Sinodi eesmärk ei ole "toota dokumente juurde", vaid "to
plant dreams, draw forth prophecies and visions, allow hope to flourish, inspire
trust, bind up wounds, weave together relationships, awaken a dawn of hope,
learn from one another and create a bright resourcefulness that will enlighten
minds, warm hearts, give strength to our hands."

Kindlasti läheb mul abi vaja.

Sina saad juba nüüd väga konkreetsel viisil kaasa aidata:
**palveta minu ja selle projekti eest.**
Väga tore oleks, kui Sa vastaksid mulle sellele kirjale, kasvõi sõnadega "Issand, võta meid kuulda".
Hea meelega loen lisaks ka Sinu isiklik julgustus või manitsus.

Teine asi, mida Sa saad juba nüüd teha: mõtle järgi, kes peaks veel selles
listis olema. Praegu oleme siin meilistis "sinodiuudised" (1) mõned varajased
kaasamõtlejaid (isa Philippe, Marge Marie, Helve, Helle Helena, Johanna, Mari)
ja (2) kõik vaimulikud, kelle emaili aadres leidsin kirikukalendrist.  Aga
teoreetiliselt peaks siin olema iga ristitud inimene, kes on nõus palvetama
selle projekti eest. See list hakkab olema meie peamine suhtluskanal.  **Kui
Sulle tuleb keegi meelde, kes võiks veel siin olla, siis edasta talle minu
kirja.**

Aitäh, et Sa lugesid lõpuni. Ja ära viivita vastamisega, sest juba varsti tuleb
järgmine kiri!

| Luc Saffre
| Laupäev, 2. oktoober 2021


.. rubric:: Viited

.. [#f1] Vatikani dokumendid on
  `Sissejuhatus <https://www.synod.va/en/news/preparatory-document.html>`__ ja
  `Vademecum <https://www.synod.va/en/news/vademecum-for-the-synod-on-synodality.html>`__,
  kokku u 70 lk. Need on olemas erinevates keeltes ja isegi audioraamatuna.
