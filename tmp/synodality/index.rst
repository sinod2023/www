================
About synodality
================


.. toctree::
    :maxdepth: 1

    /guide/synodality
    neologism
    /guide/dimensions
    /guide/perspectives
    /guide/ldp
    /guide/attitudes
    /guide/dispositions
    /guide/temptations
