===========
A neologism
===========

What means "The church is synodal"? I know that a synod is some kind of Church
council where high-level clericals decide about high-level issues. But don't
tell me that Church lives only in some assemblies of bishops!

What means "synodality"?

The word "synodal" comes from greek  *συν* (together) ja *ὁδός* (journey).
So it has the same roots as the word "synod".
But actually it is a neologism,
an independent sibling of the word "synod".
It appeared in the Catholic Church maybe only recently.

Why did this word appear and what does it mean?

Somewhere around the millenium people started to ask: How does the Church
function? The organization model (*modus vivendi et operando*) of the Church is
obviously neither democratic nor monarchistic.

A comprehensive answer to this question is being demanded with more and more urgency.
Not only because hefty internal debates raise a need for clear answers to certain faith questions.
Not only because public order demands explanations about administrative rules
and suggests optimizations for these rules.
There is another, even more important reason:
humankind as a whole struggles with exactly the same kind of challenges as the Church.
The Church obviously has a know-how about organizing our living together, and this know-how
might be useful for public administrations and governments as well.

The Church herself started only quite recently to discover that her organization
model has something special and that it deserves a name.  The word "synodality",
emerged as a name for this model.

  "What the Lord is asking of us is already in some sense present in the very
  word "Synod." Journeying together – laity, pastors, the Bishop of Rome – is an
  easy concept to put into words, but not so easy to put into practice."
  -- Pope Francis

So we can define synodality as "our way of living together". That's nice, but of
course we need to explain this in more detail.

The Church has done already some work for explaining synodality.
A number of documents have been published by the Vatican.
See the "Resources" section on our website.
Pope Francis used it during a speech on :term:`20151017`.
The first explicit document is
`Synodality in the life and mission of the Church.
<https://www.vatican.va/roman_curia/congregations/cfaith/cti_documents/rc_cti_20180302_sinodalita_en.html>`__,
a study lead by the :term:`ITC` and published in March 2018.

In 2019, German Catholics started a national project they called the "Synodal
Path". In 2021, some of them were disappointed when Francis asked them to "Calm
down, these issues aren't limited to Germany, so theses discussion must be led
by the Vatican". One can say that the Synod on Synodality is a genial answer and
continuation for the Synodal Path.

.. https://www.vaticannews.va/de/kirche/news/2021-10/deutschland-synodaler-weg-endet-vorzeitig-mangelnder-teilnehmer.html
