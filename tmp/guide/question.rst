========================
The fundamental question
========================

During the diocesan phase of the Synod, Pope Francis asks you to reflect on the
following question:

  A synodal Church, in announcing the Gospel, 'journeys together'. How is this
  'journeying together' happening today in your local Church? What steps does the
  Spirit invite us to take in order to grow in our 'journeying together?'

In order to respond, you are invited to ask yourselves what experiences in your
particular Church the fundamental question calls to mind. 

- What joys did these experiences provoke? What difficulties and obstacles have
  they encountered? What wounds have they brought to light? What insights have
  they elicited?

- Where, in these experiences, does the voice of the Spirit resound? What is he
  asking of us? What are the steps to be taken? What paths are opening up for
  our particular Church?
