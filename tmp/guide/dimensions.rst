====================================
Communion, participation and mission
====================================

By journeying together and reflecting together on the journey that has been
made, the Church will be able to learn through Her experience which processes
can help Her to live **communion**, to achieve **participation**, to open
Herself to **mission**. (PD, 1)

- **Communion** stands for the way we interact with each other in daily life
- **Participation** stands for how we interact within the hierarchy of our organization
- **Mission** stands for the way we interact with those who are outside of the Church

These three areas of activity ("levels", "dimensions") are profoundly
interrelated. They are the vital pillars of a Synodal Church. There is no
hierarchy between them. Rather, each one enriches and orients the other two.
There is a dynamic relationship between the three that must be articulated with
all three in mind.

.. inactive:

  - Communion: By his gracious will, God gathers us together as diverse peoples of
    one faith, through the covenant that he offers to his people. The communion we
    share finds its deepest roots in the love and unity of the Trinity. It is
    Christ who reconciles us to the Father and unites us with each other in the
    Holy Spirit. Together, we are inspired by listening to the Word of God,
    through the living Tradition of the Church, and grounded in the *sensus fidei*
    that we share. We all have a role to play in discerning and living out God’s
    call for his people.

  - Participation: A call for the involvement of all who belong to the People of
    God – laity, consecrated and ordained – to engage in the exercise of deep and
    respectful listening to one another. This listening creates space for us to
    hear the Holy Spirit together, and guides our aspirations for the Church of
    the Third Millennium. Participation is based on the fact that all the faithful
    are qualified and are called to serve one another through the gifts they have
    each received from the Holy Spirit. In a synodal Church the whole community,
    in the free and rich diversity of its members, is called together to pray,
    listen, analyse, dialogue, discern and offer advice on making pastoral
    decisions which correspond as closely as possible to God's will (ICT, Syn.,
    67-68). Genuine efforts must be made to ensure the inclusion of those at the
    margins or who feel excluded.

  - Mission: The Church exists to evangelize. We can never be centred on
    ourselves. Our mission is to witness to the love of God in the midst of the
    whole human family. This Synodal Process has a deeply missionary dimension to
    it. It is intended to enable the Church to better witness to the Gospel,
    especially with those who live on the spiritual, social, economic, political,
    geographical, and existential peripheries of our world. In this way,
    synodality is a path by which the Church can more fruitfully fulfil her
    mission of evangelization in the world, as a leaven at the service of the
    coming of God’s kingdom.

  (Source: :term:`Vademecum`, section 1.4)


  Different articulations of synodality
  =====================================

  In the prayer, reflection, and sharing prompted by the fundamental question, it
  is opportune to keep in mind three levels on which synodality is articulated as
  a "constitutive dimension of the Church"

  - the level of the style with which the Church **ordinarily lives** and
    works, which expresses its nature as the People of God that journeys together
    and gathers in assembly summoned by the Lord Jesus in the power of the Holy
    Spirit to proclaim the Gospel. This style is realized through “the community
    listening to the Word and celebrating the Eucharist, the brotherhood of
    communion and the co-responsibility and participation of the whole People of
    God in its life and mission, on all levels and distinguishing between various
    ministries and roles;”

  - the level of ecclesial structures and processes, determined
    also from the theological and canonical point of view, in which the synodal
    nature of the Church is expressed in an **institutional way** at the local,
    regional, and universal levels;

  - the level of synodal processes and events in which the Church is convoked by
    the competent authority, according to specific procedures determined by the
    ecclesiastical discipline.

  Although distinct from a logical point of view, these three levels refer one to
  the other and must be held together in a coherent way, otherwise a
  counter-testimony is transmitted, and the Church’s credibility is undermined. In
  fact, if it is not embodied in structures and processes, the style of synodality
  easily degrades from the level of intentions and desires to that of rhetoric,
  while processes and events, if they are not animated by an adequate style, turn
  out to be empty formalities.

  (Source: :term:`PD`, nr. 27)
