===============================
Principles of a synodal process
===============================


(Source: :term:`Vademecum`, chapter 2)

- :doc:`/intro/who`

- :doc:`ldp`

- :doc:`attitudes <attitudes>` and :doc:`dispositions <dispositions>`

- :doc:`Temptations <temptations>`
