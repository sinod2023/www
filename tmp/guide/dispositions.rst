=================================
Dispositions in a synodal process
=================================

(Source: :term:`Vademecum`, section 2.3)

The following dispositions will help participants to create a local process that
inspires people, with no one excluded to create a vision of the future filled
with the joy of the Gospel. (cf. Christus Vivit):

- An innovative outlook: To develop new approaches, with creativity and a
  certain audacity.

- Being inclusive: A participatory and co-responsible Church, capable of
  appreciating its own rich variety, embraces all those we often forget or ignore.

- An open mind: Let us avoid ideological labels and make use of all
  methodologies that have borne fruit.

- Listening to each and every one: By learning from one another, we can
  better reflect the wonderful multi-faceted reality that Christ’s Church is meant
  to be.

- An understanding of “journeying together”: To walk the path that God
  calls the Church to undertake for the third millennium.

- Understanding the concept of a co-responsible Church: To value and
  involve the unique role and vocation of each member of the Body of Christ,
  for the renewal and building up of the whole Church.

- Reaching out through ecumenical and interreligious dialogue: To dream
  together and journey with one another throughout the entire human family.
