=====================
The Church is synodal
=====================

The Catholic Church is organized in a way (*modus vivendi et operando*) that is
neither democratic, nor monarchistic, nor anarchistic. It seems that the best
word for this model is "synodal". The Church herself only starts to discover
this term.

  "Synodality is the way of being the Church today according to the will of God,
  in a dynamic of discerning and listening together to the voice of the Holy
  Spirit." -- Pope Francis
