=========================================
Listening, Discernment, and Participation
=========================================

(Source: :term:`Vademecum`, section 2.2)

The Synodal Process is first and foremost a spiritual process. It is not a
mechanical data- gathering exercise or a series of meetings and debates. Synodal
listening is oriented towards discernment. It requires us to learn and exercise
the art of personal and communal discernment. We listen to each other, to our
faith tradition, and to the signs of the times in order to discern what God is
saying to all of us. Pope Francis characterizes the two interrelated goals of
this process of listening: “to listen to God, so that with him we may hear the
cry of his people; to listen to his people until we are in harmony with the will
to which God calls us.” (:term:`20151017`)

This kind of discernment is not only a one-time exercise, but ultimately a way
of life, grounded in Christ, following the lead of the Holy Spirit, living for
the greater glory of God. Communal discernment helps to build flourishing and
resilient communities for the mission of the Church today. Discernment is a
grace from God, but it requires our human involvement in simple ways: praying,
reflecting, paying attention to one’s inner disposition, listening and talking
to one another in an authentic, meaningful, and welcoming way.

The Church offers us several keys to spiritual discernment. In a spiritual
sense, discernment is the art of interpreting in what direction the desires of
the heart lead us, without letting ourselves be seduced by what leads us to
where we never wanted to go. Discernment involves reflection and engages both
the heart and head in making decisions in our concrete lives to seek and find
the will of God.

If listening is the method of the Synodal Process, and discerning is the aim,
then participation is the path. Fostering participation leads us out of
ourselves to involve others who hold different views than we do. Listening to
those who have the same views as we do bears no fruit. Dialogue involves coming
together across diverse opinions. Indeed, God often speaks through the voices of
those that we can easily exclude, cast aside, or discount. We must make a
special effort to listen to those we may be tempted to see as unimportant and
those who force us to consider new points of view that may change our way of
thinking.
