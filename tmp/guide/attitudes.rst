================================
Attitudes in a synodal process
================================

(Source: :term:`Vademecum`, section 2.3)

On various occasions, Pope Francis has shared his vision for what the practice
of synodality looks like concretely. The following are particular attitudes that
enable genuine listening and dialogue as we participate in the Synodal Process.

- Being synodal requires time for sharing: We are invited to speak with authentic
  courage and honesty (parrhesia) in order to integrate freedom, truth, and charity.
  Everyone can grow in understanding through dialogue.

- Humility in listening must correspond to courage in speaking: Everyone has the
  right to be heard, just as everyone has the right to speak. Synodal dialogue depends
  on courage both in speaking and in listening. It is not about engaging in a debate to
  convince others. Rather, it is welcoming what others say as a way by which the Holy
  Spirit can speak for the good of all (1 Corinthians 12:7).

- Dialogue leads us to newness: We must be willing to change our opinions based
  on what we have heard from others.

- Openness to conversion and change: We can often be resistant to what the Holy
  Spirit is trying to inspire us to undertake. We are called to abandon attitudes of
  complacency and comfort that lead us to make decisions purely on the basis of how
  things have been done in the past.

- Synods are an ecclesial exercise in discernment: Discernment is based on the
  conviction that God is at work in the world and we are called to listen to what the
  Spirit suggests to us.

- We are signs of a Church that listens and journeys: By listening, the Church
  follows the example of God himself, who listens to the cry of his people. The Synodal
  Process provides us with the opportunity to open ourselves to listen in an authentic
  way, without resorting to ready-made answers or pre-formulated judgments.

- Leave behind prejudices and stereotypes: We can be weighed down by our
  weaknesses and sinfulness. The first step towards listening is freeing our minds and
  hearts from prejudices and stereotypes that lead us on the wrong path, towards
  ignorance and division.

- Overcome the scourge of clericalism: The Church is the Body of Christ filled
  with different charisms in which each member has a unique role to play. We are
  all interdependent on one another and we all share an equal dignity amidst the
  holy People of God. In the image of Christ, true power is service. Synodality
  calls upon pastors to listen attentively to the flock entrusted to their care,
  just as it calls the laity to freely and honestly express their views.
  Everyone listens to one other out of love, in a spirit of communion and our
  common mission. Thus the power of the Holy Spirit is manifested in manifold
  ways in and through the entire People of God.

- Cure the virus of self-sufficiency: We are all in the same boat. Together we
  form the Body of Christ. Setting aside the mirage of self-sufficiency, we are
  able to learn from each other, journey together, and be at the service of one
  another. We can build bridges beyond the walls that sometimes threaten to
  separate us – age, gender, wealth, ability, education, etc.

- Overcoming ideologies: We must avoid the risk of giving greater importance to
  ideas than to the reality of the life of faith that people live in a concrete way.
- Give rise to hope: Doing what is right and true does not seek to attract attention or
  make headlines, but rather aims at being faithful to God and serving His People. We
  are called to be beacons of hope, not prophets of doom.

- Synods are a time to dream and "spend time with the future": We are encouraged
  to create a local process that inspires people, with no one excluded to create
  a vision of the future filled with the joy of the Gospel. The
  :doc:`dispositions outlined on the next page <dispositions>` will help
  participants (cf. Christus Vivit).
