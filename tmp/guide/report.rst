==================
The meeting report
==================

One of the participants of a consultation meeting must do the work of writing a
report.

Without a report, your meeting was --hopefully--  very useful and you can
consider it as your participation, but your voice won't be heard at the Synod.

The core team will publish every submitted report on the `sinod.katoliku.ee
<https://sinod.katoliku.ee>`__ website, including the name of the reporter,  the
date and place where it was held, the number of participants, and of course the
statement text.

.. The name of the reporter will be published on the Estonian website.
.. All participants should agree that this is what you want to say.

It is not required but would be nice to also take pictures and submit them along
with your report (after making sure that the participants agree your pictures to
be published).

The reporter confirms that the meeting actually happened and that the statement
summarizes what the participants have to say.

Your report can be in Estonian, English or Russian.

Send your report as an email to the people on our :doc:`contact page </contact>`.


.. (See also appendix B of the :term:`Vademecum`)
