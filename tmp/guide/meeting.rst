========================
Consultation meetings
========================

In order to participate in the Synod, we ask you to meet with at least two other
persons, to exchange about the fundamental question, and then to submit a report
of your meeting.


The meetings can have different forms. The parish priests will invite the
faithful to meetings for their parish. The different church groups or
congregations can organize their own internal consultation meetings. Meetings
can also be held by independent groups, by other Christian denominations, or by
groups that are not explicitly Christian. Even a spontaneous meeting with some
friends can be a valid consultation meeting. A consultation meeting can last
more than one session.

The meetings themselves are part of the learning process and should bring true
experiences of synodality to the participants.

A typical consultation meeting might have the following elements:

- Introduction. The participants should understand why they came together.
  Use :doc:`/intro/index` for explaining the project.
  Every participant should have a :term:`questionnaire`, or point their mobile phone
  to this web page.

- Prayer. The participants pray together using the text in :doc:`/adsumus` (on
  the :term:`questionnaire`) This prayer, and your "amen" (which means "I agree")
  unites you with all  other consultation meetings in the world.

- Read :doc:`/guide/question` and optionally :doc:`/guide/nuclei` (on the
  :term:`questionnaire`)

- Optionally listen to a text from the Scriptures or to an invited guest who
  speaks about a particular topic.

- Silence. Individual time of introspection: What memories arise in me?
  How do I answer to the fundamental question?

- Exchange. Share your thoughts with the other participants.

- Decide. Which of your thoughts should go into your "statement", that is, what
  you want to say at the Synod. Your statement does not need to be long.  One
  sentence can be enough.

- Decide who will write the :doc:`meeting report <report>`.

Another form of meeting can be an expert group who write an elaborated common
statement as their input to the Synod.
