================
Two perspectives
================

It is also helpful to remember that “journeying together” occurs in two deeply
interconnected perspectives, which enrich one another and are helpful for our
common discernment towards deeper communion and more fruitful mission:

- First, we journey together with one another as the People of God.

- Next, we journey together with the entire human family, including those who do
  not call themselves People of God.

(Source: :term:`Vademecum` section 5.3 and :term:`PD` items 28-29)
