======================
The consultation phase
======================

The preparation for the Synod starts in October 2021, two years before the
actual meeting, with consultation meetings in each diocese. During this phase we
listen to what Estonia has to say at the Synod.

Key dates in Estonia:

- \ 17. October 2021: Start of the diocesan phase. Dedicated Holy Mass in every
  parish. Main celebration at 11:30 in Peter & Paul Cathedral Tallinn.

- Between October 2021 and March 2022: Consultation meetings

- April 2022: Synthesis of the results

Every submitted report of every consultation meeting will be published on this
website. And in April 2022 the core team will elaborate a synthesis of the
results because we can't expect the bishops of the Synod to read all reports of
every country. This synthesis will be a special process for which the Vatican
issued directives, but we didn't yet decide how to implement them in Estonia.

More than simply responding to a questionnaire, this phase is meant to offer as
many people as possible a truly synodal experience of listening to one another
and walking forward together, guided by the Holy Spirit.

The purpose of the Synod is not to produce documents, but "to plant dreams,
draw forth prophecies and visions, allow hope to flourish, inspire trust, bind
up wounds, weave together relationships, awaken a dawn of hope, learn from one
another and create a bright resourcefulness that will enlighten minds, warm
hearts, give strength to our hands." (PD, 32)
