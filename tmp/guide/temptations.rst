================================
Temptations in a synodal process
================================

(Source: :term:`Vademecum`, section 2.4)

As on any journey, we need to be aware of possible pitfalls that could hamper our progress
during this time of synodality. The following are several pitfalls that must be avoided in order
to promote the vitality and fruitfulness of the Synodal Process.

- The temptation of wanting to lead ourselves instead of being led by God.
  Synodality is not a corporate strategic exercise. Rather it is a spiritual
  process that is led by the Holy Spirit. We can be tempted to forget that we
  are pilgrims and servants on the path marked out for us by God. Our humble
  efforts of organization and coordination are at the service of God who guides
  us on our way. We are clay in the hands of the divine Potter (Isaiah 64:8).

- The temptation to focus on ourselves and our immediate concerns. The Synodal
  Process is an opportunity to open up, to look around us, to see things from
  other points of view, and to move out in missionary outreach to the
  peripheries. This requires us to think long-term. This also means broadening
  our perspectives to the dimensions of the entire Church and asking questions,
  such as: What is God’s plan for the Church here and now? How can we implement
  God’s dream for the Church on the local level?

- The temptation to only see “problems.” The challenges, difficulties, and
  hardships facing our world and our Church are many. Nevertheless, fixating on
  the problems will only lead us to be overwhelmed, discouraged, and cynical. We
  can miss the light if we focus only on the darkness. Instead of focusing only
  on what is not going well, let us appreciate where the Holy Spirit is
  generating life and see how we can let God work more fully.

- The temptation of focusing only on structures. The Synodal Process will naturally
  call for a renewal of structures at various levels of the Church, in order to foster
  deeper communion, fuller participation, and more fruitful mission. At the same time,
  the experience of synodality should not focus first and foremost on structures, but on
  the experience of journeying together to discerning the path forward, inspired by the
  Holy Spirit. The conversion and renewal of structures will come about only through
  the on-going conversion and renewal of all the members of the Body of Christ.

- The temptation not to look beyond the visible confines of the Church. In
  expressing the Gospel in our lives, lay women and men act as a leaven in the
  world in which we live and work. A Synodal Process is a time to dialogue with
  people from the worlds of economics and science, politics and culture, arts
  and sport, the media and social initiatives. It will be a time to reflect on
  ecology and peace, life issues and migration. We must keep the bigger picture
  in view to fulfil our mission in the world. It is also an opportunity to
  deepen the ecumenical journey with other Christian denominations and to deepen
  our understanding with other faith traditions.

- The temptation to lose focus of the objectives of the Synodal Process. As we
  proceed along the journey of the Synod, we need to be careful that, while our
  discussions might be wide-ranging, the Synodal Process maintains the goal of
  discerning how God calls us to walk forward together. No one Synodal Process
  is going to resolve all our concerns and problems. Synodality is an attitude
  and an approach of moving forward in a co-responsible way that is open to
  welcoming God’s fruits together over time.

- The temptation of conflict and division. “That they may all be one” (John
  17:21). This is the ardent prayer of Jesus to the Father, asking for unity
  among his disciples. The Holy Spirit leads us deeper into communion with God
  and one another. The seeds of division bear no fruit. It is vain to try to
  impose one’s ideas on the whole Body through pressure or to discredit those
  who feel differently.

- The temptation to treat the Synod as a kind of a parliament. This confuses
  synodality with a ‘political battle’ in which in order to govern one side must defeat the
  other. It is contrary to the spirit of synodality to antagonize others or to encourage
  divisive conflicts that threaten the unity and communion of the Church,

- The temptation to listen only to those who are already involved in Church
  activities. This approach may be easier to manage, but it ultimately ignores a
  significant proportion of the People of God.  
