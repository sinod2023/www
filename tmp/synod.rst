=====================
A Synod on Synodality
=====================

The **Synod on Synodality** is the short name for the XVI. Ordinary General
Assembly of the *Synod of Bishops*, to be held in Rome in October 2023.

The **Synod of Bishops** is an advisory body of the Catholic Church that renders
cooperative assistance to the Pope in exercising his office. Assemblies are held
every 2 to 3 years. It was created as part of the Vatican Council II, and as
such is  is a relatively new concept. (`vatican.va
<https://www.vatican.va/roman_curia/synod/index.htm>`__, `wikipedia.org
<https://en.wikipedia.org/wiki/Synod_of_Bishops_in_the_Catholic_Church>`__)

By convening this Synod, Pope Francis invites the entire Church to reflect
on a theme that is decisive for its life and mission: “For a synodal Church
– Commu­nion, Participation and Mission”.

Pope Francis insists that “it is pre­cise­ly this path of synodality which God
expects of the Church of the third millennium.” (:term:`Vademecum` 1.2)

The Synod starts in October 2021 with a "diocesan phase", during which local
teams will collect input. It will be followed by the implementation phase
that will again involve the local teams.

Official website: https://www.synod.va/en.html
