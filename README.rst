=======================
The synod project
=======================

This is the `Sphinx <http://sphinx-doc.org/>`_ source code of the content
published at <https://sinod2023.gitlab.io/www/> and <https://sinod.laudate.ee>.
