=========
Synthesis
=========

This page is work in progress.  It is planned to get approved in April 2022, the
end of the diocesan phase of the synod.

..
  Preliminary remarks (excerpts from :term:`Vademecum`):

  - The feedback received from these local gatherings will then be compiled in an
    overall synthesis at the diocesan level. The synthesis that each particular
    Church will elaborate at the end of this work of listening and discernment
    will constitute its contribution to the journey of the universal Church. To
    make the subsequent phases of the journey easier and more sustainable, it is
    important to condense the fruits of prayer and reflection into a maximum of
    **ten pages**. If necessary to contextualize and explain them better, other
    texts can be attached to support or integrate them.



  .. contents::
    :depth: 1
    :local:



  Introduction
  ============

  Our hope for this report is that it may give a *valuable* and *true* input to
  Synod on Synodality.

  This report is published as a static website with a printable summary. We expect
  the bishops who attend the synod to read at least this printable summary. Other
  parts of the website are published in the hope of giving useful background
  information to those who which to know more.

  We gave our best to focus on maximum inclusion and participation among baptized
  Catholics.

  Limitations
  ===========

  There are about 4500 Catholics living in Estonia. We are a relatively small
  group within the group of Christians in Estonia (300.000), which is itself a
  very small group within the population of Estonia (1.330.000), which is itself a
  tiny group of the world population (0.0168%).  Recommended readings: `Religion
  in Estonia <https://en.wikipedia.org/wiki/Religion_in_Estonia>`__ So please
  don't expect this report to produce statistically representative arguments for
  the future of the Church.

  .. https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population

  This report has been compiled by a volunteer who is not even Estonian.
  So please don't expect this report to be authoritative or relevant at a
  sociological, theological or political level.
