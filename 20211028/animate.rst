=====================================
How to animate a consultation meeting
=====================================

(This document is work in process. You comments are welcome.)

We need volunteers who have the gift of animating other people to speak in a
group and who agree to lead a consultation meeting.

Here are some suggestions about what to say. You may simply read the following
texts, but feel free to change the formulations, skip certain passages or to add
more explanations.  Be aware the the Holy Spirit will help you.


Introduction
============

So the pope asks us to reflect about the questions on this questionnaire.
Don't read that questionnaire right now, because that might scare you.
Let me talk to you first.

.. rubric:: About synodality

We need to understand what means "The Church is synodal". Synodality is a
neologism. It has --as far as I know-- emerged around the year 2015. It is what
we might call the **code of conduct** of the Church.  The Church itself is just
discovering this concept.

Let's look at some basic points. We can see them on our questionnaire. I will read
aloud some of the words in bold and the sentence in italics. [Don't read them
all now. Select those that seem important to you.]

The goal of our meeting is actually to learn synodality using the "Learning by
Doing" method.  We don't need to become experts in synodality.  We are not going
to read all these questions together. Synodality is actually "automatic" and
"natural" because God wants us to be synodal. But even the best experts will
never be perfectly synodal. So don't worry too much about understanding every
word. Just let us dive into it.

.. rubric:: About the report

Before we continue, I have a bad news for you. Or actually it is a good one:

One of you must do the job of writing a report. Don't expect *me* to do this. I
must lead this meeting, and it is important that another person than me writes
the report. The report is not a protocol but a synthesis. You don't need to
report *everything*. Your job as the reporter, with the help of the Holy Spirit,
is to "select" and "bring into a readable form" the things we have to say. Not
everybody is able to write a report. So if you feel a call that this job might
be done by you, then please don't be shy. There is no reason to be proud about
your gift. It's rather your ministry, by which you serve our group. We need a
reporter because without a report, our meeting will --hopefully-- very useful
and make us grow in faith and synodality, but it won't be heard at the Synod.
The reporter confirms that the meeting actually happened.

The core team will publish every submitted report on the `sinod.katoliku.ee
<https://sinod.katoliku.ee>`__ website, together with the date and place where
it was held and the number of participants. So the reporter will take care about
not publishing anything that is confidential. The core team will also check for
potential "leaks" of confidential information and will not publish such reports
(and instead ask the reporter to formulate things in an anonymous way).

I will not collect your contact data. If you want to see the report before the
reporter sends it out, then give your contact data to the reporter. You may also
simply watch for our report to appear on the sinod website.

It might happen that you read our published report some days after the meeting
and feel that something important is missing. In that case you can attend to
another meeting and say it again, in the hope that the reporter won't miss your
point.

It would be nice to also take pictures and submit them along with your report.
But this is not required, and if only one of us here feels unsure about being on
a public picture on a website: just say it now. It is really understandable and
no problem.

We don't need to decide about the reporter right now. I will explain a few more
things and will ask again later.

.. rubric:: About "hot topics"

When asked for input to the Synod, a natural reaction is to discuss about
certain controversial questions (communion with other confessions, marriage,
divorce, abortion, ...).

While such discussions are welcome and natural, keep in mind that the Synod is
not about *these topics*. The Synod is about *synodality*, i.e. about *how we
speak about* these topics.

Each of us here probably has their own personal opinion about what is true and
important. And some of us probably have very opposing opinions. And still we all
are here in the name of God. We cannot exclude anybody. This is synodality. We
are together, and we want to advance together. Synodality is how to organize
Church on her journey of seeking what is good and true, given the fact that all
baptized are called to advance together united in diversity.

Keep in mind that we do not need to make any decisions here. We do not even need
to give advice. Believe me: the pope has many high-level experts to advise him,
and these experts are selected in order to represent every possible opinion. For
each of these hot topics there are many books written about it. Let's not waste
our time with reinventing the wheel.  There is a more important job for us to
do:

.. rubric:: We need true stories

This is another important thing I must tell you.

The most valuable input for the Synod are *true* stories about *real*
experiences from *your* life and with *your* emotions.

"This and this happened to me and I felt like this."

Telling real stories needs more courage and trust than formulating your opinion
or advice regarding some exciting topic.

You might think that telling your own story is "egocentric" or a sign of "pride".
And we read that pride is a capital sin.
But there is a difference between arrogance and gratefulness.
**You** are the only one who can tell your story.
If **you** don't tell it, nobody will ever tell it.
Synodality is when no cry and no joy dies in your heart before you have shared it to somebody else.

Telling stories is actually easier that discussing about hot topics.
And it brings you more joy.
I would even say that we all actually *long*,
deep in our heart,
for sharing the stories of our lives with somebody.

Some of you maybe already have a "best friend", i.e. somebody who listens to all
your stories. In that case you are a lucky person.

I won't push you to tell any story.
I will maybe try to encourage you.
But you may, at any moment, simply say that you prefer to be silent.

Consultation
================

.. rubric:: Prayer

Now let's pray.

So this prayer on your questionnaire unites us with all the other humans in the world
who are responding to the Pope's call. Please consider this! Isn't this great?

(Read the prayer either one line per participant, or all together)

.. rubric:: The fundamental question

Okay, let's start!

Let us just read together the main question and the explanations below it.

.. rubric:: Silence

The next step is to be silent. Let's say that we meet again here in 10 minutes,
at XXX o'clock. You can stay here in the room, or go out and have a walk. You
can read all these questions if you want, or you can take a bible and read a
text from the Scriptures, or you can just listen to your heart.

After the silence:

Okay, time is over. Who is the first to tell us a story?

.. rubric:: Exchange

(Now your job is to let the people speak. Try to not influence them. Try to
encourage everybody. Try to maintain a friendly and calm atmosphere.)

Ending the meeting
==================

At some moment you should announce "Let's say that we stop this consultation in
20 minutes". Set your alarm clock so that everybody hears when time is over.

During the last five minutes: calm down.

Remind the important job of the reporter.
Invite every participant to pray for the reporter.

To conclude the meeting, pray the Adsumus once more. Pray an Our Lord. Pronounce
a blessing.
