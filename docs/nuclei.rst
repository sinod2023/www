========================
Kümme temaatilist tuuma
========================

Põhiküsimuse üle mõtisklemisel võivad olla abiks järgmised teemad. Need toovad
välja mõned sinodaalsuse olulised aspektid ja neid võib kasutada mõttevahetuse
ja dialoogi lähtepunktina. Arutleda võib aga ka muude teemade üle.

.. Kogemuste esiletoomiseks ja konsultatsioonile rohkem kaasa aitamiseks toome
  allpool välja kümme temaatilist tuuma, mis väljendavad „elatud sinodaalsuse”
  erinevaid tahke.

**1. TEEKAASLASED**

*Kirikus ja ühiskonnas oleme kõrvuti samal teel.*

Kes on teie kohalikus kirikus need, kes on ühisel teekonnal? Kui me ütleme „meie
kirik”, siis kes on selle osa? Kes palub meil koos teel olla? Kes on
teekaaslased, sealhulgas väljaspool kirikuala? Millised isikud või rühmad
jäetakse avalikult või ka varjatult kõrvale?


**2. KUULAMINE**

*Kuulamine on esimene samm, kuid see nõuab avatud meelt ja südant, ilma eelarvamusteta.*

Kuidas Jumal räägib meiega nende kaudu, kelle häält me mõnikord ei võta kuulda?
Kuidas kuulatakse ilmikuid, eriti noori ja naisi? Kuidas me kaasame pühendunud
meeste ja naiste panuse? Milline koht on vähemuste, kõrvalejäetute ja tõrjutute
häälel? Kas tunneme ära eelarvamused ja stereotüübid, mis takistavad meid
kuulamast? Kuidas me kuulame ühiskondlikku ja kultuurilist konteksti, milles me
elame?

**3. JULGELT RÄÄKIMINE**

*Kõik on kutsutud rääkima julgelt ja otsekoheselt, see tähendab vabaduses, tões ja armastuses.*

Kuidas edendame kogukonnas ja selle organisatsioonides vaba ja siirast
suhtlusstiili, ilma kahepalgelisuse ja omakasupüüdlikkuseta? Ja seoses
ühiskonnaga, mille osa me oleme? Millal ja kuidas me suudame öelda, mis on meie
jaoks oluline? Kuidas toimib suhe meediaga (mitte ainult katoliku meediaga)? Kes
räägib kristliku kogukonna nimel ja kuidas ta valitakse?

**4. PÜHITSEMINE**

*Ühine teekond on võimalik ainult siis, kui see põhineb Sõna ühisel kuulamisel ja Armulaua pühitsemisel.*

Kuidas palve ja liturgia pühitsemine meie ühist teekonda inspireerivad ja
suunavad? Kuidas need inspireerivad kõige olulisemaid otsuseid? Kuidas edendame
kõigi ustavate aktiivset osalemist liturgias ja pühitsusfunktsiooni täitmisel?
Millist ruumi antakse lugeja ja altariteenri teenistustele?


**5. KAASVASTUTAMINE MISSIOONIS**

*Sinodaalsus teenib Kiriku missiooni, milles on kutsutud osalema kõik tema liikmed.*

Kuna me kõik oleme misjonijüngrid, siis kuidas kutsutakse iga ristitud inimest
missiooni peategelaseks? Kuidas toetab kogukond oma liikmeid, kes on pühendunud
ühiskonnas teenimisele (ühiskondlik ja poliitiline pühendumine, teadusuuringud
ja õpetamine, sotsiaalse õigluse edendamine, inimõiguste kaitse ja ühise kodu
eest hoolitsemine jne)? Kuidas aitate neil täita neid kohustusi missioonile
toetudes? Kuidas tehakse otsuseid missiooniga seotud valikute kohta ja kes
selles osaleb? Kuidas integreeritakse ja kohandatakse sinodaalsel viisil
erinevaid traditsioone, mis moodustavad paljude, eriti idakirikute pärandi,
pidades silmas tõhusat kristlikku tunnistust?  Kuidas toimib koostöö Ukraina
Kreeka-Katoliku kirikuga ja teiste *sui iuris* kiri­ku­tega?


**6. DIALOOG KIRIKUS JA ÜHISKONNAS**

*Dialoog on üksteisemõistmise tee, mis nõuab ka püsivust ja kannatlikkust, kuid mis on võimeline koguma inimeste ja rahvaste kogemusi.*

Millised on dialoogi kohad ja viisid meie kirikus? Kuidas käsitletakse
lahknevusi visioonides, konflikte ja raskusi? Kuidas edendame  koostööd
naaberpiiskopkondadega, piirkonna usukogukondadega ja nende vahel, ilmalike
ühenduste ja liikumistega ja nende vahel jne? Millised kogemused dialoogist ja
jagatud pühendumusest on meil teiste religioonide usklike ja mitteusklikega?
Kuidas peab kirik dialoogi teiste ühiskonna sektoritega ja neist õpib:
poliitika, majandus, kultuur, kodanikuühiskond, vaesed...?

**7. TEISTE KRISTLIKE KONFESSIOONIDEGA**

*Dialoogil erinevate konfessioonide kristlastega, ke­da ühendab üks ristimine, on sinodaalsel teekonnal eriline koht.*

Millised suhted on meil teiste kristlike konfessioonide vendade ja õdedega?
Milliseid valdkondi need puudutavad? Milliseid vilju oleme sellest ühisest
teekonnast saanud? Millised on raskused? Kuidas astuda järgmisi samme üheskoos
edasi?


**8. AUTORITEET JA OSALEMINE**


*Sinodaalne kirik on kaasav ja kaasvastutav kirik.*

Kuidas me tuvastame taotletavad eesmärgid, nende saavutamise viisi ja vajalikud
sammud? Millised on meeskonnatöö ja kaasvastutamise tavad? Kuidas edendatakse
ilmikute teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad
sinodaalsed organid konkreetse kiriku tasandil? Kas need on viljakad kogemused?

.. Helve:
  Kuidas määrame taotletavad eesmärgid, nende saavutamise viisid ja astutavad
  sammud? Kuidas toimub valitsemine konkreetselt meie Kirikus? Millised on
  koostöö ja kaasvastutamise tavad? Kuidas soodustatakse ilmikute
  teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad sinodaalsed
  kogud konkreetse Kiriku tasandil? Kas need on tulemuslikud kogemused?


**9. ÄRATUNDMINE JA OTSUSTAMINE**

*Sinodaalses laadis tehakse otsuseid äratundmise põhjal, mis põhineb konsensusel, mis lähtub ühisest kuulekusest Vaimule.*

Milliste protseduuride ja meeto­ditega me koos äratundmisele jõuame ja otsuseid
teeme? Kuidas saab neid parandada? Kuidas edendame otsustamises osalemist
hierarhiliselt struktureeritud kogukondades? Kuidas me seome konsultatiivse
etapi aruteluga, otsuse tegemise protsessi otsuse vastuvõtmise hetkega? Kuidas
ja milliste vahenditega edendame läbipaistvust ja aruandlust?

.. Helve:

  - eristamine ja otsustamine -- Sinodaalselt toimides tehakse otsuseid eristamise
    kaudu, tuginedes konsensusele, mis lähtub ühisest kuulekusest Vaimule.

    Milliste protseduuride ja meetoditega eristame koos ja teeme otsuseid? Kuidas
    saaks neid täiendada? Kuidas me soodustame otsustamises osalemist
    hierarhiliselt korraldatud kogukondades? Kuidas me ühendame omavahel
    konsultatiivse etapi ja kaalutlemisetapi, otsuse tegemise protsessi ja otsuse
    tegemise hetke? Kuidas ja milliste vahenditega soodustame läbipaistvust ja
    vastutavust?



**10.\  KUJUNEMINE SINODAALSUSES**

*Ühise teekonna vaimsusest on kutsutud kujunema kasvatuslik põhimõte inimese ja kristlase, perekondade ja kogukondade kujunemisel.*

Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
vastutusrollid, et nad suudaksid paremini ühisel teekonnal käia, üksteist
kuulates ja dialoogi pidades? Millist kasvatust pakume äratundmise ja
autoriteedi rakendamiseks? Millised vahendid aitavad meil mõista meid ümbritseva
kultuuri dünaamikat ja selle mõju meie kirikule?

.. Helve:
  - täienda ja kasvata -- koos rändamise vaimsus on kutsutud saama inimisiku ja kristlase, perekondade ja kogukondade kujundamise kasvatuslikuks põhimõtteks.

    Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
    vastutav roll, et nad oleksid veelgi suutelisemad "koos rändama", üksteist
    kuulates ja dialoogi pidades? Millist kujundamist pakume eristamise
    hõlbustamiseks ja sinodaalselt valitsemiseks? Millised vahendid aitavad meil
    mõista ümbritsevas kultuuris toimivaid jõude ja nende mõju meie laadi
    Kirikule?



(Allikas: :term:`ED` punkt 30 ja :term:`Vademecum` punkt 5.3)
