=======
Kontakt
=======

Palun saatke kohtumiste aruanded, kommentaarid, küsimused ja tagasiside
aaddressile:

    sinod@katoliku.ee

Teie e-posti aadress edastatakse sinoditiimile, kus on kuus liiget: Luc Saffre
(sinodireferent), Marge Paas (meediareferent), isa Edgaras Versockis, Helle
Helena Puusepp, Helve Trumann ja Johanna Aus.

Sinoditiim tegutseb sinodaalselt piiskop Philippe Jourdani volitusel, kes määrab
tiimi liikmed.

Kui soovite tiimiga ühineda või sinodile oma panuse anda, võtke palun ühendust
Luc Saffre'ga.
