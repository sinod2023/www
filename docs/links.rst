===========
Lugemist
===========

.. contents::
  :local:


Uudised Eestis
==============

.. include:: /include/uudised.rst


Piiskoppide Sinodi avaldatud dokumendid
=======================================

.. glossary::

  ED

    Ettevalmistusdokument.
    `Vatikani originaal
    <http://www.synod.va/en/documents/versione-desktop-del-documento-preparatorio.html>`__.
    :doc:`Tõlge eesti keelde <pd>`.

  Vademecum

    `Vademecum <http://www.synod.va/en/documents/vademecum.html>`__,
    a 22-page handbook for the diocesan phase of the synod published by the Vatican in September 2021.

  SynDCP

    `The Diocesan Contact Person(s)/Team <https://www.synod.va/content/dam/synod/document/common/phases/Diocesan-Contact-Person.pdf>`__

  SynDiscerning

    `Discerning the path for your diocese <https://www.synod.va/content/dam/synod/document/common/phases/Discerning-the-path-for-your-diocese.pdf>`__

  SynTools

    `The Suggested tools for reflecting, sharing, and responding to the
    questions of the Synod <https://www.synod.va/content/dam/synod/document/common/phases/Suggested-tools-for-reflecting-sharing.pdf>`__

  Presentation slides

    A series of `presentation slides <https://docs.google.com/presentation/d/1eWKgnrKYgsepuo0DMBbCSQEuweo_ZdqI/edit#slide=id.p6>`__



Vatikani avaldatud dokumendid
=============================

.. glossary::

  LG

    `Lumen gentium <https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19641121_lumen-gentium_en.html>`__.
    Dogmatic constitution on the Church.
    Vatican Council II.
    (21 November 1964)

  UR

    `Unitatis redintegratio <https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_decree_19641121_unitatis-redintegratio_en.html>`__
    Decree on ecumenism.
    Vatican Council II.
    (21 November 1964)

  DV

    `Dei Verbum <https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19651118_dei-verbum_en.html>`__.
    Dogmatic constitution on divine revelation.
    Vatican Council II.
    (18 November 1965)

  GS

    `Gaudium et spes
    <https://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19651207_gaudium-et-spes_en.html>`__,
    Pastoral Constitution on the Church in the modern world.
    Vatican Council II
    (7 December 1965)

  1983CIC

    `1983 Code of Canon Law <https://en.wikipedia.org/wiki/1983_Code_of_Canon_Law>`__
    (*Codex Iuris Canonici*),
    the current fundamental body of ecclesiastical laws for the Latin Church.

  LF

    `Lumen fidei <https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20130629_enciclica-lumen-fidei.html>`__
    Encyclical letter
    of the Holy Father Francis
    on faith.
    (29 June 2013)

  EG

    `Evangelii gaudium <https://www.vatican.va/content/francesco/en/apost_exhortations/documents/papa-francesco_esortazione-ap_20131124_evangelii-gaudium.html>`__
    Apostolic exhortation
    of the Holy Father Francis
    on the proclamation of the Gospel in today's world.
    (24 November 2013)

  LS

    `Laudato si’ <https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20150524_enciclica-laudato-si.html>`__
    Encyclical letter
    of the Holy Father Francis
    on care for our common home.
    (24 May 2015)

  20151017

    Franciscus, `Address at the Ceremony Commemorating the 50 th Anniversary
    of the Institution of the Synod of Bishops
    <https://www.vatican.va/content/francesco/en/speeches/2015/october/documents/papa-francesco_20151017_50-anniversario-sinodo.html>`__
    (17 oktoboer 2015).

  20180302

    `Synodality in the life and mission of the Church <https://www.vatican.va/roman_curia/congregations/cfaith/cti_documents/rc_cti_20180302_sinodalita_en.html>`__,
    a study lead by the :term:`ITC` and published (2 March 2018)

  EC

    `Episcopalis communio <https://www.vatican.va/content/francesco/en/apost_constitutions/documents/papa-francesco_costituzione-ap_20180915_episcopalis-communio.html>`__
    Apostolic constitution of the Holy Father Francis.
    (15 September 2018)

  FT

    `Fratelli tutti <https://www.vatican.va/content/francesco/en/encyclicals/documents/papa-francesco_20201003_enciclica-fratelli-tutti.html>`__
    Encyclical letter
    of the Holy Father Francis
    on fraternity and social friendship.
    (3 October 2020)
