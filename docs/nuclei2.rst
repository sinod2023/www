=================================
Kümme temaatilist tuuma (variant)
=================================

.. include:: /include/wip.rst

Oma kogemuse paremaks kajastamiseks mõtle järgmistele aspektidele, mille kaudu Kirik elab oma sinodaalset elu.

1. TEEKAASLASED –  Kes on need, kellega tunned end oma koguduses olevat ühisel teekonnal? Kui me ütleme: „Meie kirik”, siis millest see koosneb? Kelle kutsel me ühisel teekonnal oleme? Kes on teekaaslased, ka väljaspool meie kirikuruumi? Millised isikud või rühmad on jäänud eraldiseisvaks, isegi eemalolevaks? Kas see on tingitud nende soovist või tõrjutusest?

2. KUULAMINE – Kuulamine ei tähenda veel kuulmist. Et kuulda, on vaja eelarvamustest vaba avatud meelt ja südant. Keda konkreetne kirik peab kuulama? Kuidas kuulatakse ilmikuid? Kas võetakse kuulda naisi ja noori inimesi? Kuidas integreeritakse koguduse ellu pühitsetud meeste ja naiste arvamus? Kas on kuulda ka vähemuste või kõrvalejäetute hääli? Kas on võimalik tuvastada eelarvamusi ja stereotüüpe, mis takistavad meil kuulmist? Kuidas me tajume sotsiaalset ja kultuurilist konteksti, milles me elame?

3. JULGUS RÄÄKIDA – Kõik on kutsutud julgelt ja avameelselt rääkima.  See tähendab kõnelemist integreerides vabadust, tõde ja armastust. Kuidas edendame oma kogukonnas ja selle organisatsioonides vaba ja autentset suhtlusstiili, ilma kahepalgelisuse ja oportunismita? Kas meie hääl kõlab ka ühiskonnas, mille osa me oleme? Millal ja kuidas suudame öelda, mis on meie jaoks oluline? Kuidas toimub suhtlemine meediaga (mitte ainult katoliku meediaga)? Kes räägib kristliku kogukonna nimel ja kuidas on need inimesed valitud?

4. PÜHITSEMINE – Ühist teekonda on võimalik käia ainult siis, kui see põhineb Sõna ühisel kuulamisel ja Armulaua pühitsemisel. Kuidas palve ja liturgiline pühitsemine inspireerivad ja suunavad meie ühist teekonda? Kuidas kujunevad nende toel kõige olulisemaid otsused? Kuidas edendada kõigi ustavate aktiivset osalemist liturgias? Kuidas valitakse sõnaliturgia lugeja ja kes määrab akolüüdi funktsiooni täitjad?

5. KAASVASTUTUS KIRIKU MISSIOONIS – Sinodaalsus teenib Kiriku missiooni, milles kõik tema liikmed on kutsutud osalema. Kuna me kõik oleme misjonäridest jüngrid, siis kuidas kutsutakse iga ristitud inimest missioonis osalema? Mis takistab ristitul kiriku missioonis osalemist? Milliseid missiooni valdkonnad on jäetud hooletusse ? Kuidas toetab kogukond oma liikmeid, kes teenivad ühiskonda erinevatel viisidel (sotsiaalne ja poliitiline kaasamine, teadusuuringud, haridus, sotsiaalse õigluse edendamine, inimõiguste kaitsmine, keskkonna eest hoolitsemine jne)?  Kuidas tehakse otsuseid, mis on seotud missiooniga ja kes nende otsuste tegemisel osalevad? Kuidas integreeritakse ja kohandatakse erinevaid traditsioone? Kuidas väljendatakse kristlikku tunnistust teiste kirikute ja idamaa pärandi suhtes? Kuidas toimib koostöö piirkondades, kus on palju erinevaid iseseisvaid kirikud?

6. DIALOOG KIRIKUS JA ÜHISKONNAS – Dialoog on püsivuse tee, mis tähendab ka vaikuse aegu ja kannatusi, kuid mille tulemusena jõutakse talletatud kogemuseni. Millised on dialoogi kohad ja viisid meie kirikus? Kuidas käsitletakse lahknevusi visioonides ning lahendatakse konflikte ja raskusi? Kuidas edendada koostööd naaberpiiskopkondade, erinevate kohalike usukogukondade ning ilmalike ühenduste ja liikumistega vahel ? Millised on kogemused dialoogist ja jagatud pühendumusest teiste religioonide usklike ja mitteusklikega? Kuidas peab kirik dialoogi ja õpib teistest ühiskonna sektoritest: poliitikast, majandusest, kultuurist, kodanikuühendustest?

7. DIALOOG TEISTE KRISTLIKE KONFESSIOONIDEGA – Dialoogil erinevate kristlike konfessioonidega, mida ühendab üks ristimine, on sinodaalsel teekonnal eriline koht. Millised suhted on meil teiste kristlike konfessioonide vendade ja õdedega? Milliseid valdkondi need puudutavad? Milliseid vilju oleme sellest ühisest teekonnast ammutanud? Millised on raskused?

8. AUTORITEET JA OSALEMINE – Sinodaalne kirik on kaasav ja kaasvastutav kirik. Kuidas me teeme kindlaks ja sõnastame oma eesmärgid, nende saavutamise viisid ja vajaliku sammud? Millised tavad on kujunenud meeskonnatöös ja ühises vastutuses? Kuidas edendatakse ilmikute teenistusi ja ustavate vastutuse võtmist? Kuidas toimivad sinodaalsed organisatsioonid konkreetse kiriku tasandil? Kas need on viljakad kogemused?

9. ÄRATUNDMINE JA OTSUSTAMINE – Sinodaalses laadis tehtud otsused on konsensuslikud ning lähtuvad ühisest kuulekusest Pühale Vaimule. Milliste viisil jõuame ühisele  äratundmisele ja otsuste tegemiseni? Kuidas neid viise parandada? Kuidas edendame otsuste tegemisel osalemist hierarhiliselt struktureeritud kogukondades? Millist liigendust rakendatakse konsultatiivse etapi arutelust otsuse tegemise hetkeni? Kuidas ja milliste vahenditega tagatakse otsuste tegemise läbipaistvus ja aruandlus?

10. SINODAALSUSE KUJUNEMINE– Sinodaalsus tähendab valmisolekut muutusteks, pidevat õppimist, oskust ära kuulata ning dialoogi pidada. Kuidas toetab ja kujundab meie kristlik kogukond inimesi, kes oleksid võimelised võtma vastu vastutaja rolli, et muuta neid võimekamateks. Millist moodust oleme valmis pakkuma neile oma autoriteedi ja otsustusvõime avaldamiseks sinodaalsel viisil? Millised meetodid aitavad meil mõista selle kultuuriruumi dünaamikat, mille keskel me elame ja milline on selle mõju meie Kirikule?
