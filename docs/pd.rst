=====================
Ettevalmistusdokument
=====================

Eestikeelne versioon :term:`ettevalmistusdokumendist <ED>` (tõlge: Tiiu Tälli ja
sinoditiim).

.. contents::
   :depth: 2
   :local:


Sinodaalse Kiriku jaoks: osadus, osalemine ja missioon
======================================================

(1) Jumala Kirik kutsutakse kokku sinodiks. Tee pealkirjaga „Sinodaalse Kiriku
jaoks: osadus, osalus ja missioon“ avatakse pidulikult 9.-10. oktoobril 2021
Roomas ja järgneval 17. oktoobril igas konkreetses kirikus. Üks põhiline etapp
on piiskoppide sinodi XVI korralise üldkogu tähistamine oktoobris 2023,[1]
millele järgneb rakendusetapp, mis hõlmab taas konkreetseid kirikuid (vrd EC,
art. 19–21). Selle kokkukutsumisega kutsub Paavst Franciscus kogu Kirikut
mõtisklema Kiriku elu ja missiooni jaoks määrava teema üle: „Just seda
sinodaalsuse teed ootab Jumal kolmanda aastatuhande Kirikult.”[2] See teekond,
mis järgneb Vatikani II kirikukogu pakutud Kiriku „uuendamisele,“ on nii
kingitus kui ka ülesanne: üheskoos rännates ja läbitud teekonna üle koos
mõtiskledes saab Kirik oma kogemuste kaudu õppida, millised protsessid võivad
aidata tal elada osaduses, saavutada osalust ja avada end missioonile. Meie
„ühine teekond” on tegelikult see, mis kõige tõhusamalt kehastab ja avaldab
Kiriku kui Jumala rahva palveränduri ja misjonäri olemust.

(2) Meid õhutab ja juhatab põhiküsimus: kuidas võimaldab see „ühine teekond”,
mis toimub täna erinevatel tasanditel (kohalikust tasandist universaalsele),
Kirikul kuulutada evangeeliumi vastavalt talle usaldatud missioonile; ja
milliseid samme kutsub Vaim meid astuma, et kasvada sinodaalse Kirikuna?

Selle küsimuse koos käsitlemine nõuab Püha Vaimu kuulamist, kes nagu meeldib
tuul „puhub kuhu tahab, ja sa kuuled ta häält, kuid te ei tea, kust ta tuleb ja
kuhu ta läheb“ (Jh 3:8), jäädes avatuks üllatustele, mida Vaim kindlasti
valmistab meie jaoks ette. Seega aktiveerub dünaamika, mis võimaldab meil hakata
lõikama mõningaid sinodaalse pöördumise vilju, mis hakkavad järk -järgult
küpsema. Need on kirikliku elu kvaliteedi ja evangeliseerimise missiooni
täitmiseks väga olulised eesmärgid, milles me kõik osaleme oma ristimise ja
kinnitamise kaudu. Siin nimetame peamised põhimõtted, mis väljendavad
sinodaalsust kui Kiriku vormi, stiili ja struktuuri:

• meenutamine, kuidas Vaim on juhatanud Kiriku teekonda läbi ajaloo ja kutsub täna meid koos olema Jumala armastuse tunnistajad;
• elamine osalemist soodustavas ja kaasavas kiriklikus protsessis, mis pakub kõigile - eriti neile, kes on erinevatel põhjustel ääremaale sattunud - võimaluse väljendada end ja olla ära kuulatud, et aidata kaasa Jumala rahva edendamisele;
• Vaimu poolt kogukonna heaks ja kogu inimeste pere hüvanguks heldelt antavate jõukuse ja kingituste ja karismade mitmekesisuse tunnustamine ja hindamine;
• osalusviiside otsimine vastutuse kandmiseks evangeeliumi kuulutamisel ja püüdluses ilusama ja elamiskõlblikuma maailma loomiseks;
• vastutuse ja võimu kasutamise jälgimine Kirikus, samuti struktuuride, mille abil neid juhitakse, tuues päevavalgele ja püüdes ümber pöörata eelarvamusi ja moonutatud tavasid, mis ei ole juurdunud evangeeliumis;
• kristliku kogukonna akrediteerimine usaldusväärse subjekti ja kindla partnerina sotsiaaldialoogi, tervenemise, leppimise, kaasamise ja osalemise, demokraatia ülesehitamise, vendluse ja sotsiaalse sõpruse edendamise teedel;
• suhete taasloomine kristlike kogukondade liikmete vahel, samuti kogukondade ja teiste sotsiaalsete rühmade vahel, nt teiste konfessioonide ja religioonidega usklike kogukonnad, kodanikuühiskonna organisatsioonid, rahvaliikumised jne;


(3) Viimaste sinodaalsete kogemuste viljade tunnustamise ja omaksvõtmise
edendamine üldisel, piirkondlikul, riiklikul ja kohalikul tasandil. See
ettevalmistusdokument on sinodaalse teekonna teenistuses, eriti vahendina, mis
hõlbustab Jumala rahva kuulamise ja temaga konsulteerimise esimest etappi
konkreetsetes kirikutes (oktoober 2021 - aprill 2022), lootuses aidata käivitada
kõigi teekonnal osalejate ideed, energia ja loovus ning hõlbustada nende
pingutuste viljade jagamist. Selle eesmärgiga: 1) alustatakse mõningate
kaasaegse konteksti silmapaistvate joonte väljatoomisega; 2) illustreeritakse
sünteetiliselt põhilisi teoloogilisi viiteid sinodaalsuse õigeks mõistmiseks ja
praktiseerimiseks; 3) pakutakse mõningaid piiblimõtteid, mis võivad toita
mediteerimist ja palvete rohket mõtisklust; 4) illustreeritakse mõningaid
vaatenurki, mille järgi sinodaalsuse kogemusi uuesti lugeda; 5) näidatakse
mõningaid viise, kuidas seda uuesti lugemist sõnastada palvetes ja jagamisel.
Töö konkreetse korraldamise toeks pakutakse käesolevale ettevalmistavale
dokumendile lisatud metoodilist käsiraamatut, mis on ka kättesaadav spetsiaalsel
veebisaidil [3]. Sait pakub mõningaid vahendeid sinodaalsuse teema
süvendamiseks, toetades seda ettevalmistavat dokumenti; nende hulgast tahaksime
esile tuua kahte, mida on mitu korda allpool mainitud: kõne piiskoppide sinodi
asutamise 50. aastapäeva tseremoonial, mille pidas paavst Franciscus 17.
oktoobril 2015, ja dokument „Sinodaalsus Kiriku elus ja missioonis“ (Synodality
in the Life and Mission of the Church), mille on koostanud Rahvusvaheline
Teoloogiline Komisjon ja mis avaldati 2018. aastal.


I. Kutse ühisele teekonnale
===========================

(4) Sinodaalne teekond rullub lahti ajaloolises kontekstis, mida iseloomustavad
ühiskonnas toimuvad epohhiloovad muutused ja oluline muutus Kiriku elus, millest
ei saa mööda vaadata: just selle konteksti keerukuse voltides, selle pingetes ja
vastuoludes oleme kutsutud „uurima aja märke ja tõlgendama neid evangeeliumi
valguses“ (GS, nr 4). Siin on toodud mõned ülemaailmse stsenaariumi elemendid,
mis on kõige tihedamalt seotud sinodi teemaga; aga pilti tuleb kohalikul
tasandil rikastada ja täiendada.


(5) Selline ülemaailmne tragöödia nagu COVID-19 pandeemia „taaselustas hetkeks
tunde, et oleme globaalne kogukond, kõik ühes paadis, kus ühe inimese probleemid
on kõigi probleemid. Taas saime aru, et üksinda ei päästeta kedagi; meid saab
päästa vaid koos“ (FT, nr 32). Samal ajal on pandeemia pannud ka plahvatama juba
olemasoleva ebavõrdsuse ja ebavõrdsuse: tundub, et inimkonda raputavad üha enam
massi- ja killustumisprotsessid; traagiline olukord, millega rändajad silmitsi
seisavad kõikides maailma piirkondades, näitab, kui kõrged ja tugevad on tõkked,
mis eraldavad ühte inimeste peret. Entsüklikad Laudato si’ ja Fratelli Tutti
dokumenteerivad inimkonda läbivate murrangute sügavust, ja me võime neile
analüüsidele tuginedes hakata kuulama vaeste ja maa hüüdeid ning tunda ära
lootuse ja tuleviku seemneid, mida Vaim üha külvab ka meie ajal: „Looja ei jäta
meid maha; ta ei hülga kunagi oma armastusplaani ega kahetse, et on meid loonud.
Inimkonnal on endiselt võime teha koostööd meie ühise kodu ülesehitamiseks“ (LS,
nr 13).

(6) See olukord, mis vaatamata suurtele erinevustele ühendab kogu inimperekonda,
esitab väljakutse Kiriku võimele olla kaaslaseks üksikisikutele ja
kogukondadele, et taas teada saada leina- ja kannatamiskogemustest, mis on
paljastanud palju valekindlusi, ning kasvatada lootust ja usku Looja ja tema
loomingu headusse. Siiski ei saa me peita end asjaolu eest, et Kirik peab ise
silmitsi seisma usupuuduse ja korruptsiooniga enda sees. Eelkõige ei saa
unustada alaealiste ja haavatavate inimeste kannatusi, mis on põhjustatud
„seksuaalse väärkohtlemise, võimu kuritarvitamise ja südametunnistuse
kuritarvitamise tõttu, mille on toime pannud märkimisväärne arv vaimulikke ja
pühitsetud isikuid” [4]. Meile esitatakse pidevalt väljakutseid „Jumala rahvana
kanda oma vendade ja õdede valu, kes on haavatud lihast ja vaimust” [5]. Liiga
kaua on ohvrite karje olnud karje, mida Kirik pole suutnud piisavalt kuulda.
Need on sügavad haavad, mida on raske ravida ja mille eest ei saa kunagi
andestust piisavalt paluda ning mis on takistuseks, mõnikord ka kohutavaks,
edasiliikumisel “ühise teekonna” suunas. Kogu Kirik on kutsutud toime tulema oma
ajaloost päritud klerikalismist läbiimbunud kultuuri raskusega ja nende
volituste teostamise vormidega, millele on külge poogitud erinevat tüüpi
kuritarvitamist (võimu, majanduslikku, südametunnistuse, seksuaalset). On
võimatu mõelda „meie kui Kiriku tegevuse muutmisest, mis ei hõlma kõigi Jumala
rahva liikmete aktiivset osalemist:” [6] üheskoos palugem Issandalt „pöördumise
armu ja sisemist võidmist, mis on vajalik, et väljendada oma süümepiina nende
kuritarvitamise kuritegude ees ja otsustavust julgelt nende vastu võidelda” [7].

(7) Hoolimata meie truudusetusest tegutseb Vaim jätkuvalt ajaloos ja näitab oma
eluandvat väge. Uued usukeeled ja uued teed õitsevad just inimpere ja Jumala
rahva kannatuste poolt välja kaevatud vagudes: suutes mitte ainult tõlgendada
sündmusi teoloogilisest vaatenurgast, vaid ka leida katsumustes põhjusi, miks
kristlik ja kiriklik elutee uuesti üles leida. Suurt lootust tekitab, et enam
kui mõned kirikud on juba alustanud enam-vähem struktureeritud kohtumisi ja
konsultatsioone Jumala rahvaga.  Seal, kus neid on tähistanud sinodaalne stiil,
on kirikutunne õitsenud ja kõigi osalemine on andnud kiriklikule elule uue hoo.
Kinnitatakse ka noorte soovi olla kirikus peategelased ning naiste ja abikaasade
suuremat tunnustust Kiriku missioonis osalemise eest, millest andsid märku juba
2018. ja 2019. aasta sinodaalkogud. Selles suunas liiguvad ka hiljutine
katehheesi õpetaja ilmaliku teenistuse institutsioon ning juurepääsu avamine
naistele lugeja ja akolüüdi ilmalikele teenistustele.

(8) Me ei saa eirata erinevaid tingimusi, milles kristlikud kogukonnad elavad
erinevates maailma piirkondades. Lisaks riikidele, kus Kirik tervitab enamikku
elanikkonnast ja kujutab endast kultuurilist võrdluspunkti kogu ühiskonnale, on
ka teisi, kus katoliiklased on vähemus; mõnes neist riikidest kogevad
katoliiklased koos teiste kristlastega tagakiusamist, sealhulgas väga
vägivaldset, ja mitte harva märtrisurma. Kui ühelt poolt ilmalik mentaliteet
kipub religiooni avalikust ruumist välja ajama, siis teisest küljest toidab
religioosne fundamentalism, austamata teiste vabadusi, sallimatuse ja vägivalla
vorme, mis kajastuvad ka kristlikus kogukonnas ja suhetes ühiskonnaga.
Kristlased võtavad mitte harva samu hoiakuid, isegi õhutavad lõhesid ja
vastuseisu, sealhulgas Kirikus. Samavõrd on vaja arvestada Kirikus ja selle
suhetes ühiskonnaga järelkaja mõradest, mis on põhjustatud rahvusest, rassist,
kastist või muudest sotsiaalse kihistumise vormidest või kultuurilisest ja
struktuurilisest vägivallast, mis viimast läbivad. Nendel olukordadel on sügav
mõju väljendi „ühine teekond” tähendusele ja selle tegelikule võimalusele.

(9) Selles kontekstis on sinodaalsus peamine tee Kiriku jaoks, kes on kutsutud
end uuendama Vaimu toimel ja Sõna kuulates. Võimalus ette kujutada Kiriku ja
tema institutsioonide teistsugust tulevikku vastavalt tema missioonile sõltub
suuresti otsusest algatada kuulamis-, dialoogi- ja kogukonna
otsustamisprotsessid, milles igaüks saab osaleda ja kaasa aidata. Samas on
„ühise teekonna“ otsus prohvetlik märk inimperele, kes vajab ühist projekti, mis
suudab taotleda kõigi huve. Kirik, olles võimeline osadust ja vendlust,
osalemist ja subsidiaarsust järgima, truuduses sellele, mida ta kuulutab, suudab
seista vaeste ja vähimate kõrval ning laenata neile oma häält. “Ühiseks
teekonnaks” peame laskma end Vaimul harida tõeliselt sinodaalseks
mentaliteediks, sisenedes julguse ja südamevabadusega pöördumisprotsessi, mis on
hädavajalik „pidevaks reformimiseks, mida [Kirik] alati vajab, kuivõrd ta on
iniminstitutsioon” (UR, nr 6; vrd EG, nr 26).


II. Olemuslikult sinodaalne Kirik
=================================

(10) „Just seda sinodaalsuse teed ootab Jumal kolmanda aastatuhande Kirikult.
See, mida Issand meilt palub, on mõnes mõttes juba olemas sõnas „sinod”, [8] mis
on „iidne ja auväärne sõna Kiriku traditsioonis, mille tähendus tugineb Ilmutuse
sügavamatele teemadele”.[9] „Issand Jeesus esitleb end „tee, tõe ja eluna ”(Jh
14: 6)” ja „Kristlasi, Tema järgijaid, nimetati algselt „Tee järgijaiks” (vrd Ap
9:2; 19:9,23; 22:4; 24:14,22).”[10] Selles mõttes on sinodaalsus palju enamat
kui kiriklike koosolekute ja piiskoppide kokkutulekute tähistamine või lihtne
sisemine asjaajamine Kirikus;  see on „Kiriku, Jumala rahva eriline modus
vivendi et operandi, mis ilmutab ja annab sisu tema osadusele, kui kõik tema on
ühisel teekonnal, kogunevad kokku ja osalevad aktiivselt tema
evangeliseerimismissioonis.” [11] Nii on põimunud sinodi pealkirjaga pakutud
sinodaalse Kiriku peateljed: osadus, osalemine ja missioon. Selles peatükis
näitame lühidalt mõningaid fundamentaalseid teoloogilisi viiteid, millel see
vaatenurk põhineb.

(11) Esimesel aastatuhandel oli „ühine teekond” - see tähendab sinodaalsuse
praktiseerimine - tavaline viis, kuidas Kirik, mida mõisteti kui „inimesi, kes
olid ühinenud Isa ja Poja ja Püha Vaimu ühtsuses,” [12] tegutses. Neile, kes
lõid kiriklikus tervikus lõhesid, Kirikuisad seadsid vastu kogu maailmas
hajutatud kirikute osaduse  To those who were creating divisions in the
ecclesial body, the Church Fathers opposed the communion of the Churches
scattered throughout the world, mida  kirjeldas Püha Augustinus kui
“concordissima fidei conspiratio” [13], see tähendab kõigi ristitute kokkulepe
usus. Siin on sinodaalse praktika laia arengu juured Kiriku elu kõigil
tasanditel - kohalikul, maakondlikul ja universaalsel -, mis saavutas oma
kõrgeima ilmingu oikumeenilises kirikukogus. Sellel kiriklikul horisondil,
inspireerituna põhimõttest, et kõik osalevad Kiriku elus, võis Püha Johannes
Krisostomus öelda, et „Kirik ja sinod on sünonüümid” [14]. Isegi teisel
aastatuhandel, kui kirik rõhutas tugevamalt hierarhilist funktsiooni, ei lakanud
selline tegutsemisviis: kui oikumeeniliste kirikukogude ning piiskopkondade ja
maakondade sinodite pidamisega on see hästi tõendatud, siis ka dogmaatiliste
tõdede määratlemisel soovisid paavstid konsulteerida piiskoppidega, et teada
saada kogu Kiriku usu kohta, pöördudes kogu Jumala rahva sensus fidei
autoriteedile, mis on „eksimatu „in credendo”(EG, nr 119).


(12) Vatikani II kirikukogu kinnitub sellele traditsiooni dünaamikale.
Kirikukogu rõhutab, et „Jumal ei tee aga inimesi pühaks ega päästa neid ainult
üksikisikutena, ilma ühenduste ja sidemeteta üksteise vahel. Pigem on Tal olnud
hea meel ühendada inimesed üheks rahvaks, rahvaks, kes tunnistab Teda tões ja
teenib Teda pühaduses.” (LG, nr 9). Jumala rahva liikmeid ühendab ristimine ja
„kui Kristuse tahtel saavad mõned teiste nimel õpetajateks, pastoriteks ja
saladuste jagajateks, jagavad siiski kõik tõelist võrdsust väärikuse ja kõigi
ustavate ühise tegevuse osas Kristuse Ihu ülesehitamiseks” (LG, nr 32). Seetõttu
on kõik ristitud, Kristuse preestrilistes, prohvetlikes ja kuninglikes
ülesannetes osalejad, „kasutades oma karisma, kutsumuse ja teenistuse
mitmekesisust ja korrapärast rikkust”, [15] aktiivsed evangeliseerimise kandjad,
nii individuaalselt kui ka kogu Jumala rahvana.

(13) Kirikukogu rõhutas, kuidas ristimisel saadud Püha Vaimu võidmise tõttu ei
saa ustavate tervik „usuasjades eksida. Nad ilmutavad seda erilist omadust kogu
rahva üleloomuliku taju abil usu küsimustes, kui nad „alates piiskoppidest kuni
viimase ilmikust usklikuni” näitavad üldist nõusolekut usu ja moraali küsimustes
”(LG, nr 12).  See on Vaim, kes juhib usklikke „kogu tõesse” (Jh 16:13). Vaimu
tegutsemise kaudu „areneb see apostlitelt pärinev traditsioon Kirikus,” et
Jumala rahvas kasvaks „arusaamises tegelikkusest ja edasi antud sõnadest. See
juhtub mõtisklemise ja vaatlemise kaudu, mida teevad usklikud, kes hindavad neid
asju oma südames (vrd Lk 2:19, 51), läbitungiva arusaamise kaudu vaimsetest
reaalsustest, mida nad kogevad, ja jutluste kaudu nende poolt, kes on
piiskoppide järgluse kaudu saanud tõe kindla anni”(DV, nr 8).Tegelikult peab see
pastorite poolt kokku kogutud rahvas kinni Jumala Sõna pühast hoiust, mis on
usaldatud Kirikule, püsib pidevalt apostlite õpetuses, leivamurdmises ja palves,
„nii et usupärandist kinni pidamine, praktiseerimine ja tunnistamine muutub
piiskoppide ja ustavate osas ühtseks ühiseks pingutuseks” (DV, nr 10).


(14) Pastorid, kelle Jumal on kehtestanud kui „autentsed eestkostjad, tõlgid ja
kogu Kiriku usu tunnistajad, ”[16] ei peaks kartma kuulata neile usaldatud
karja. Jumala rahvaga konsulteerimine ei tähenda, et Kirikus eeldatakse
demokraatia dünaamikat, mis põhineb enamuse põhimõttel, sest igas sinodaalses
protsessis osalemise aluseks on jagatud kirg evangeliseerimise ühise missiooni
vastu ning mitte vastandlike huvide esindamine. Teisisõnu, see on kiriklik
protsess, mis saab toimuda ainult „hierarhiliselt struktureeritud kogukonna
keskmes”. [17] Just viljakas sidemes Jumala rahva sensus fidei ja pastorite
õpetusfunktsiooni vahel saab teoks kogu Kiriku ühehäälne konsensus ühes usus.
Iga sinodaalne protsess, mille käigus piiskoppe kutsutakse mõistma, mida Vaim
Kirikule ütleb, kuulates mitte iseenesest, vaid Jumala rahvast, kes „osaleb ka
Kristuse prohvetlikus tegevuses” (LG, nr. 12), on ilmne vorm sellest „ühisest
teekonnast”, mis paneb Kiriku kasvama. Püha Benedictus rõhutab, kuidas „Issand
ilmutab sageli kõige mõistlikuma kursi, mida järgida,” [18] neile, kes
kogukonnas olulisi positsioone ei täida (sel juhul noorimad); seega peaksid
piiskopid hoolitsema selle eest, et jõuaksid igaüheni, et sinodaalse teekonna
korrepärasel avanemisel saaks täide minna, mida apostel Paulus kogukondadele
soovitab: „Ärge kustutage Vaim, ärge põlastage prohvetiandi, katsuge läbi kõik,
pidage kinni heast” (1Ts 5:19-21).


(15) Teekonna mõte, kuhu me kõik oleme kutsutud, on ennekõike sinodaalse Kiriku
näo ja vormi avastamine, milles „kõigil on midagi õppida. Ustav rahvas,
piiskoppide kolleegium, Rooma piiskop: kõik kuulavad üksteist ja kõik kuulavad
Püha Vaimu, „tõe Vaimu” (Jh 14:17), et teada saada, mida Ta „kogudustele ütleb”
(Ilm 2: 7).”[19] Rooma piiskop kui Kiriku ühtsuse põhimõte ja alus palub kõiki
piiskoppe ja kõiki konkreetseid kirikuid, milles ja millest eksisteerib üks ja
ainus Katoliku Kirik (vrd LG, nr 23), siseneda enesekindlalt ja julgelt
sinodaalsuse teele. Sellel „ühisel teekonnal” palume Vaimul aidata meil
avastada, kuidas osadus, mis toob kokku ühtsuses mitmesuguseid ande, karismaid
ja teenistusi, on mõeldud missiooniks: sinodaalne Kirik on Kirik, mis „läheb
edasi”, misjonikirik, „mille uksed on avatud” (EG, nr 46). See hõlmab üleskutset
süvendada suhteid teiste kirikute ja kristlike kogukondadega, kellega meid
ühendab üks ristimine. „Ühise teekonna” perspektiiv on seega veelgi laiem ja
hõlmab kogu inimkonda, kelle „rõõme ja lootusi, leinasid ja muresid” me jagame
(GS, nr 1). Sinodaalne Kirik on prohvetlik märk ennekõike rahvaste kogukonnale,
kes ei suuda välja pakkuda ühist projekti, mille kaudu püüelda kõigi heaolu
poole: sinodaalsuse praktiseerimine on tänapäeval Kiriku jaoks kõige ilmsem viis
olla „päästmise universaalne sakrament” (LG, nr 48), „märk ja vahend intiimsest
ühinemisest Jumalaga ja kogu inimkonna ühtsusest” (LG, nr 1).


III. Pühakirja kuulamine
========================

(16) Jumala Vaim, kes valgustab ja elavdab seda koguduste „ühist teekonda”, on
seesama Vaim, kes töötab Jeesuse missioonil, tõotatud apostlitele ja jüngrite
põlvkondadele, kes kuulevad Jumala Sõna ja rakendavad seda. Vaim ei piirdu
Issanda tõotuse kohaselt Jeesuse evangeeliumi järjepidevuse kinnitamisega, vaid
valgustab tema ilmutuse üha uusi sügavusi ja inspireerib otsuseid, mis on
vajalikud Kiriku teekonna säilitamiseks (vrd Jh 14:25-26; 15:26-27; 16:12-15).
Seetõttu on kohane, et meie teekond sinodaalse kiriku ehitamiseks oleks
inspireeritud kahest Pühakirja “kujutisest”. Üks ilmneb evangeeliumiteekonnaga
pidevalt kaasneva „kogukondliku stseeni” kujutamises; teine viitab Vaimu
kogemusele, milles Peetrus ja algkogukond tunnistavad ohtu seada usu jagamisele
põhjendamatud piirid. Ühise teekonna sinodaalne kogemus, järgides Issandat ja
kuuletudes Vaimule, suudab saada otsustavat inspiratsiooni ilmutuse nende kahe
tunnuse üle mõtiskledes.

Jeesus, rahvahulk, apostlid
---------------------------

(17) Algne stseen ilmub oma põhistruktuuris konstantsena viisil, kuidas Jeesus
ilmutab end läbi kogu evangeeliumi, kui ta kuulutab Jumala Kuningriigi tulekut.
Sisuliselt on kaasatud kolm tegelast (pluss üks). Esimene on muidugi Jeesus,
absoluutne peategelane, kes võtab initsiatiivi ja külvab Kuningriigi tuleku sõnu
ja märke „olemata erapoolik” (vrd Ap 10:34). Jeesus pöörab erinevatel viisidel
erilist tähelepanu neile, kes on Jumalast “eraldatud” ja kogukonna poolt
“hüljatud” (patused ja vaesed, evangeeliumi keeles).

Oma sõnade ja tegudega pakub ta Jumal Isa nimel ja Püha Vaimu väes vabanemist
kurjast ja pöördumist lootusesse. Isegi Issanda kutsete mitmekesisuses, nende
vastuvõtlikes vastustes on ühine joon see, et usk tuleb alati esile kui inimeste
väärtustamine: nende palvet kuuldakse, nende raskusi aidatakse, nende
kättesaadavust hinnatakse, nende väärikust kinnitab Jumala pilk ja taastub
kogukonna tunnustus.

(18) Tegelikult poleks evangeliseerimise töö ja päästesõnum mõistetavad, kui
Jeesus ei oleks pidevalt avatud võimalikult laiale publikule, mida evangeeliumid
nimetavad rahvahulgaks, see tähendab kõiki inimesi, kes järgivad teda sellel
teel ja mõnikord isegi jälitavad teda märgi ja päästesõna lootuses: see on teine
tegelane Ilmutuse stseenis. Evangeeliumi kuulutamine ei ole adresseeritud ainult
vähestele valgustatud või valitud inimestele. Jeesuse vestluskaaslane on tavaelu
„rahvas”, inimliku seisundi „igaüks”, kelle ta seab otseselt kokku Jumala anni
ja päästekutsega. Tunnistajaid üllataval ja mõnikord skandaali tekitaval viisil
võtab Jeesus vestluspartneriteks kõik need, kes rahvahulgast välja tulevad: ta
kuulab Kaanani naise kirglikke etteheiteid (vrd Mt 15:21-28), kes ei saa leppida
sellega, et ta on välja arvatud õnnistusest; ta lubab endale dialoogi Samaaria
naisega (vrd Jh 4:1-42), vaatamata tema seisundile sotsiaalselt ja religioosselt
kompromiteeritud naisena; ta palub vaba ja tänulikku usutegu pimedana sündinud
mehelt (vrd Jh 9), kelle ametlik religioon oli armu piiridest välja jätnud.

(19) Mõned järgivad Jeesust otsesemalt, kogedes jüngriks olemise truudust, samas
kui teisi kutsutakse tagasi oma tavalise elu juurde: ometi tunnistavad nad kõik
neid päästnud usu väest (vrd Mt 15:28). Nende seas, kes järgivad Jeesust, tuleb
selgelt esile apostlite kuju, keda ta ise nimetab algusest peale, olles andnud
neile ülesandeks vahendada autoriteetselt rahvahulga suhet Ilmutuse ja Jumala
Kuningriigi tulekuga. Kolmas näitleja siseneb stseenile mitte tänu ravile või
pöördumisele, vaid seetõttu, et see langeb kokku Jeesuse kutsega. Apostlite
valimine ei ole ainuüksi eksklusiivse võimu ja eraldatuse positsiooni privileeg,
vaid õnnistuse ja vendluse kaasava teenistuse arm. Tänu Ülestõusnud Issanda
Vaimu annile peavad nad valvama Jeesuse kohta, asendamata teda: mitte panema
filtreid tema kohalolekule, vaid hõlbustama temaga kohtumist.

(20) Jeesus, rahvahulk oma mitmekesisuses, apostlid: see on kujundlikkus ja
saladus, mida tuleb pidevalt kaaluda ja põhjalikult uurida, et Kirik saaks üha
enam selleks, kes ta on. Ükski kolmest tegelaset ei saa sündmuskohalt lahkuda.
Kui Jeesus puudub ja tema asemele astub keegi teine, saab Kirikust siis lepingu
apostlite ja rahvahulga vahel ning tema dialoog jõuab lõpuks poliitilise mängu
süžeeni. Ilma Jeesuse poolt volitatud ja Vaimu juhendatud apostliteta on suhe
evangeelse tõega katki ja rahvahulk, olgu see siis Jeesuse omaks võtmine või
tagasilükkamine, jääb tema kohta käiva müüdi või ideoloogia alla. Ilma
rahvahulgata saab apostlite suhe Jeesusega rikutud sektantlikuks ja enesele
viitavaks usuvormiks ja evangeliseerimine, mis lähtub otsesest eneseilmutusest,
millega Jumal isiklikult kõigi poole pöördub, pakkudes oma päästet, kaotab oma
valguse.

(21) Siis on “ekstra” tegelane, antagonist, kes toob sündmuskohale ülejäänud
kolme saatanliku eraldamise. Seistes silmitsi häiriva väljavaatega ristile, on
lahkuvaid jüngreid ja meeleolumuutvaid rahvahulki. Salakavalus, mis lahutab - ja
seega takistab ühist teed - avaldub ilmtingimata religioosse ranguse, moraalse
ettekirjutuse näol, mis on esitleb end nõudlikumana kui Jeesuse oma, ja maise
poliitilise tarkuse ahvatluse kujul, mis väidab end olevat tõhusama kui
vaimudest arusaamine. „Neljanda tegelase” pettustest pääsemiseks on vajalik
pidev pöördumine. Sellega seoses on sümboolne kohordipealik Korneeliuse lugu
(vrd Ap 10), selle Jeruusalemma „kirikukogu” eelkäija (vrd Ap 15), mis on
sinodaalse Kiriku jaoks ülioluline võrdluspunkt.

Pöördumise kahekordne dünaamika: Peetrus ja Korneelius (Ap 10)
--------------------------------------------------------------

(22) Episood jutustab ennekõike Korneeliuse pöördumisest, kes saab isegi
omamoodi kuulutuse. Korneelius on pagan, arvatavasti roomlane, okupatsiooniarmee
kohordipealik (madala auastmega ohvitser), kes tegeleb vägivallal ja
väärkohtlemisel põhineva elukutsega. Ometi on ta pühendunud palvele ja almuste
andmisele, see tähendab, et ta arendab suhet Jumalaga ja hoolib oma ligimesest.
Just tema koju siseneb ingel üllatuslikult, kutsub teda nimepidi ja manitseb
teda saatma - missiooni verb! - oma sulased Joppesse kutsuma - kutsumuse verb! -
Peetrust. Seejärel muutub jutustus looks viimase pöördumisest, kes samal päeval
sai nägemuse, milles hääl käskis tal tappa ja süüa loomi, kellest osa olid
ebapuhtad. Tema vastus on otsustav: "Ei ilmaski, Issand!” (Ap 10:14). Ta
mõistab, et Issand räägib temaga, kuid keeldub kindlalt, kuid ta keeldub
kindlalt, sest see korraldus lammutab Toora ettekirjutused, mis on tema
religioosse identiteedi jaoks võõrandamatud, ja mis väljendab võimalust mõista
valimist kui erinevust, mis sisaldab teistest rahvastest eraldamist ja
tõrjutust.

(23) Apostel on endiselt sügavalt häiritud ja samal ajal, kui ta mõtleb juhtunu
tähenduse üle, jõuavad kohale Korneeliuse saadetud mehed ja Vaim näitab talle,
et nad on tema saadikud. Peetrus vastab neile sõnadega, mis meenutavad Jeesuse
aias öelduid: „Mina olen see, keda te otsite” (Ap 10:21). See on tõeline ja
korralik pöördumine, oma kultuuri- ja usukategooriatest lahkumise valus ja
tohutult viljakas kulg: Peetrus nõustub sööma koos paganatega toitu, mida ta oli
alati keelatuks pidanud, tunnistades seda elu ja ühenduse osaks Jumalaga ja
teistega. Just inimestega kohtumisel, nende tervitamisel, nendega reisimisel ja
nende kodudesse sisenemisel mõistab ta oma nägemuse tähendust: ükski inimene
pole Jumala silmis vääritu, ja valimisega määratud erinevus ei tähenda
ainuõiguslikku eelistamist, vaid teenimist ja universaalse avaruse tunnistamist.

(24) Nii Korneelius kui ka Peetrus kaasavad oma teekonnale teisi inimesi muutes
neid teekonna kaaslasteks. Apostellik tegevus täidab Jumala tahet, luues
kogukonna, lõhkudes tõkkeid ja edendades kohtumisi. Sõna mängib kahe peategelase
kohtumises keskset rolli. Korneelius alustab oma kogemuse jagamisega. Peetrus
kuulab teda ja räägib siis, teatades omakorda, mis temaga juhtus, ja tunnistades
Issanda lähedust, kes läheb inimestega kohtuma, et vabastada nad sellest, mis
teeb nad kurja vangideks ja tapab inimkonna (vt Ap 10:38). See suhtlusviis
sarnaneb sellele, mida Peetrus kasutab Jeruusalemmas, kui ümberlõigatud usklikud
teda kritiseerivad, süüdistades teda traditsiooniliste normide rikkumises,
millele kogu nende tähelepanu näib keskenduvat, jättes samal ajal tähelepanuta
Vaimu väljavalamise: " Sina oled astunud sisse ümberlõikamata meeste juurde ja
söönud koos nendega!" (Ap 11:3). Sel konfliktihetkel teatab Peetrus, mis temaga
juhtus ning oma hämmelduse, arusaamatuse ja vastupanu reaktsioonidest. Just see
aitab tema vestluspartneritel, esialgu agressiivsetel ja jäikadel, juhtunut
kuulata ja sellega leppida. Pühakiri aitab tõlgendada tähendust, nagu seda
tehakse ka Jeruusalemma „kirikukogus”, otsustusprotsessis, mis seisneb Vaimu
üheskoos kuulamises.

IV. Sinodaalsus tegudes: teed Jumala rahvaga konsulteerimiseks
==============================================================

(25) Sõnast valgustatud ja traditsioonile rajatud sinodaalne tee on juurdunud
Jumala rahva konkreetsesse ellu. Tegelikult esitab see eripära, mis on ka
erakordne ressurss: selle objekt - sinodaalsus - on ka selle meetod. Teisisõnu,
see kujutab endast omamoodi ehitusplatsi või pilootkogemust, mis võimaldab kohe
hakata lõikama vilju sellest dünaamikast, mida progressiivne sünodaalne
pöördumine kristlikku kogukonda toob. Teisest küljest võib see viidata ainult
sinodaalsuse kogemustele erinevatel tasanditel ja erineva intensiivsusega: nende
tugevad küljed ja saavutused annavad väärtuslikke elemente edasise liikumissuuna
eristamiseks, sama teevad ka nende ppirangud ja raskused. Loomulikult viidatakse
siinkohal praeguse sinodaalse teekonna aktiveeritud kogemustele, aga ka kõigile
neile, kus „ühise teekonna” vorme juba tavaelus kogetakse, isegi kui mõistet
sinodaalsus ei teata ega kasutata.

Põhiküsimus
-----------

(26) Põhiküsimus, mis juhib seda konsulteerimist Jumala rahvaga, nagu alguses
mainitud, on järgmine:

  Sinodaalne Kirik on evangeeliumi kuulutades ühisel teekonnal: Kuidas see ühine
  teekond täna teie konkreetses kirikus toimub? Milliseid samme kutsub Vaim meid
  astuma, et kasvada oma ühisel teekonnal?

Vastamiseks palume teil:

a) küsida endalt, milliseid kogemusi teie konkreetses kirikus põhiküsimus meelde tuletab;

b) lugeda neid kogemusi sügavamalt uuesti: milliseid rõõme nad tekitasid?
Milliste raskuste ja takistustega nad on kokku puutunud? Milliseid haavu on nad
päevavalgele toonud? Milliseid teadmisi nad on tekitanud?

c) koguda vilju, et neid jagada: Kus nendes kogemustes Vaimu hääl kostab? Mida
   ta meilt küsib? Millised punktid tuleb kinnitada, millised on muutuse
   väljavaated, milliseid samme tuleb võtta? Kus me konsensuse registreerime?
   Millised teed avanevad meie konkreetse kiriku jaoks?


Sinodaalsuse erinevad liigendused
---------------------------------

(27) Põhiküsimuse ajendatud palves, järelemõtlemises ja jagamises on otstarbekas
meeles pidada kolme tasandit, millel sinodaalsus on sõnastatud kui „Kiriku
olemuslik mõõde” [20].


◦ Kiriku tavalise elu- ja tööstiili tasand, mis väljendab oma olemust Jumala rahvana, kes rändab koos ja koguneb Issanda Jeesuse poolt kokku kutsutud kogudusse Püha Vaimu väes evangeeliumi kuulutama.  See stiil realiseerub „kogukonna kaudu, kes kuulab Sõna ja tähistab Armulauda, See stiil realiseerub „osaduse vennaskonna ning kogu Jumala rahva kaasvastutuse ja osalemise kaudu oma elus ja missioonis, kõigil tasanditel ning erinevate teenistuste ja rollide vahel;“ [21]

◦ ka teoloogilisest ja kanoonilisest vaatenurgast määratud kiriklike struktuuride ja protsesside tasand, milles Kiriku sinodaalne olemus väljendub institutsionaalsel viisil kohalikul, piirkondlikul ja universaalsel tasandil;

◦ sinodaalsete protsesside ja sündmuste tasand, kus pädev asutus kutsub Kiriku kokku kirikliku distsipliini määratud konkreetsete menetluste kohaselt.

Ehkki need kolm tasandit on loogilisest vaatenurgast erinevad, viitavad need
üksteisele ja neid tuleb sidusalt kokku hoida, vastasel juhul edastatakse
vastutunnistus ja õõnestatakse Kiriku usaldusväärsust. Tegelikult, kui seda ei
rakensdata struktuurides ja protsessides, halveneb sinodaalsuse stiil hõlpsalt
kavatsuste ja soovide tasemelt retoorika tasemele, samas kui protsessid ja
sündmused, kui neid ei elavda vastav laad, osutuvad tühjadeks formaalsusteks.

(28) Lisaks tuleb kogemuste uuesti lugemisel meeles pidada, et „ühist teekonda”
saab mõista kahest erinevast vaatenurgast, mis on omavahel tihedalt seotud.
Esimene vaatenurk vaatleb konkreetsete kirikute siseelu, nende osade vahelistes
suhetes (ennekõike ustavate ja nende pastorite vahel, ka kanoonilise
distsipliini kavandatud osalusorganite, sealhulgas piiskopkonna sinodi kaudu) ja
kogukondades, kuhu nad jagunevad (eriti kogudustes). Seejärel kaalub ta
piiskoppide ja Rooma piiskopi vahelisi suhteid, ka sinodaalsuse vaheorganite
kaudu (Patriarhaalsete ja peamiste peapiiskopkonna kirikute piiskoppide sinodid,
kirikute hierarhide nõukogud ja kirikute hierarhide koosolekud sui iuris ning
piiskoppide konverentsid koos nende rahvuslike, rahvusvaheliste ja
kontinentaalsete väljendustetega). Seejärel laieneb see viisidele, kuidas iga
konkreetne kirik integreerib endasse kloostri-, religioosse ja pühitsetud elu,
ilmalike ühenduste ja liikumiste, eri liiki kiriku- ja kiriklike
institutsioonide (koolid, haiglad, ülikoolid, sihtasutused, heategevus- ja
abiorganisatsioonid jne) erinevate vormide panuse. Lõpuks hõlmab see vaatenurk
ka suhteid ja ühiseid algatusi teiste kristlike konfessioonide vendade ja
õdedega, kellega jagame sama ristimise andi.


(29) Teine vaatenurk käsitleb seda, kuidas Jumala rahvas on teel koos kogu
inimperega. Seega keskendub meie pilk suhete, dialoogi ja võimalike ühiste
algatuste olukorrale teiste religioonide usklikega, inimestega, kes on
usukauged, samuti teatud sotsiaalsete keskkondade ja rühmadega, nende
institutsioonidega (poliitika-, kultuuri-, majandus-, rahandus-, töömaailm,
ametiühingud ja äriühendused, valitsusvälised ja kodanikuühiskonna
organisatsioonid, üldrahvalikud liikumised, mitmesugused vähemused, vaesed ja
tõrjutud jne).


Kümme temaatilist tuuma, mida uurida
------------------------------------

(30) Kogemuste esiletoomise ja konsultatsioonile rohkem kaasa aitamiseks toome
allpool välja kümme temaatilist tuuma, mis väljendavad „elatud sinodaalsuse”
erinevaid tahke. Neid tuleks kohandada erinevate kohalike kontekstidega ning
aeg-ajalt neid integreerida, selgitada, lihtsustada ja süvendada, pöörates
erilist tähelepanu neile, kellel on raskem osaleda ja vastata: käesolevale
ettevalmistavale dokumendile lisatud käsiraamat pakub tööriistu, marsruute ja
soovitusi, et erinevad küsimuste rühmad saaksid konkreetselt inspireerida
palve-, kujunemis-, järelemõtlemis- ja vahetushetki.


**1. TEEKAASLASED**

*Kirikus ja ühiskonnas oleme samal teel kõrvuti.*

Kes on teie kohalikus kirikus need, kes on “ühisel teekonnal”? Kui me ütleme:
„meie kirik”, siis kes on selle osa? Kes palub meil koos teel olla? Kes on
teekaaslased, sealhulgas väljaspool kirikuala? Millised isikud või rühmad jäävad
äärealadele, nimelt või tegelikult?

**2. KUULAMINE**

*Kuulamine on esimene samm, kuid see nõuab avatud meelt ja südant, ilma eelarvamusteta.*

Keda meie konkreetne kirik „peab kuulama”? Kuidas kuulatakse ilmikuid, eriti
noori ja naisi? Kuidas me integreerime pühitsetud meeste ja naiste panust?
Milline koht on vähemuste, kõrvalejäetute ja tõrjutute häälel? Kas tuvastame
eelarvamused ja stereotüübid, mis takistavad meid kuulamast? Kuidas me kuulame
sotsiaalset ja kultuurilist konteksti, milles me elame?

**3. JULGELT RÄÄKIMINE**

*Kõik on kutsutud julgelt ja kammitsematult rääkima, see tähendab vabadust,
tõde ja armastustt integreerides.*

Kuidas edendame kogukonnas ja selle organisatsioonides vaba ja autentset
suhtlusstiili, ilma kahepalgelisuse ja oportunismita? Ja seoses ühiskonnaga,
mille osa me oleme? Millal ja kuidas me suudame öelda, mis on meie jaoks
oluline? Kuidas toimib suhe meediasüsteemiga (mitte ainult katoliku meediaga)?
Kes räägivad kristliku kogukonna nimel ja kuidas neid valitakse?

**4. PÜHITSEMINE**

*“Ühine teekond” on võimalik ainult siis, kui see põhineb Sõna ühisel kuulamisel ja Armulaua pühitsemisel.*

Kuidas palve ja liturgiline pühitsemine inspireerivad ja suunavad meie „ühist
teekonda”? Kuidas nad inspireerivad kõige olulisemaid otsuseid?  Kuidas edendada
kõigi ustavate aktiivset osalemist liturgias ja pühitsusfunktsiooni täitmisel?
Millist ruumi antakse lugeja ja akolüüdi teenistustele?

**5. KAASVASTUTAMINE MISSIOONIS**

*Sinodaalsus teenib Kiriku missiooni, milles kõik tema liikmed on kutsutud osalema.*

Kuna me kõik oleme misjonijüngrid, siis kuidas kutsutakse iga ristitud inimest
missiooni peategelaseks? Kuidas toetab kogukond oma liikmeid, kes on pühendunud
ühiskonnas teenimisele (sotsiaalne ja poliitiline pühendumine, teadusuuringud ja
õpetamine, sotsiaalse õigluse edendamine, inimõiguste kaitse ja ühise kodu eest
hoolitsemine jne)? Kuidas aitate neil missiooni loogikas neid kohustusi täita?
Kuidas tehakse otsuseid missiooniga seotud valikute kohta ja kes selles osaleb?
Kuidas on integreeritud ja kohandatud erinevad traditsioonid, mis moodustavad
paljude kirikute, eriti idamaade, pärandi, seoses sinodaalse laadiga, silmas
pidades tõhusat kristlikku tunnistust? Kuidas toimib koostöö piirkondades, kus
on erinevad sui iuris kirikud?

**6. DIALOOG KIRIKUS JA ÜHISKONNAS**

*Dialoog on püsivuse tee, mis hõlmab ka vaikust ja kannatusi, kuid mis on
võimeline koguma inimeste ja rahvaste kogemusi.*

Millised on dialoogi kohad ja viisid meie kirikus? Kuidas käsitletakse
lahknevusi visioonides, konflikte ja raskusi? Kuidas edendada koostööd
naaberpiiskopkondadega, piirkonna usukogukondadega ja nende vahel, ilmalike
ühenduste ja liikumistega ja nende vahel jne? Millised kogemused dialoogist ja
jagatud pühendumusest on meil teiste religioonide usklike ja mitteusklikega?
Kuidas peab kirik dialoogi ja õpib teistest ühiskonna sektoritest: poliitika,
majandus, kultuur, kodanikuühiskond ja vaesed…?

**7. TEISTE KRISTLIKE KONFESSIOONIDEGA**

*Dialoogil erinevate kristlike konfessioonidega, mida ühendab üks ristimine, on
sinodaalsel teekonnal eriline koht.*

Millised suhted on meil teiste kristlike konfessioonide vendade ja õdedega?
Milliseid valdkondi need puudutavad? Milliseid vilju oleme sellest „ühisest
teekonnast” saanud? Millised on raskused?

**8. AUTORITEET JA OSALEMINE**

*Sinodaalne kirik on kaasav ja kaasvastutav kirik.*

Kuidas me tuvastame taotletavad eesmärgid, nende saavutamise viisi ja vajalikud
sammud? Millised on meeskonnatöö ja kaasvastutamise tavad? Kuidas edendatakse
ilmikute teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad sinodaalsed
organid konkreetse kiriku tasandil? Kas need on viljakad kogemused?

.. Helve:
  Kuidas määrame taotletavad eesmärgid, nende saavutamise viisid ja astutavad
  sammud? Kuidas toimub valitsemine konkreetselt meie Kirikus? Millised on
  koostöö ja kaasvastutamise tavad? Kuidas soodustatakse ilmikute
  teenimisameteid ja ustavate vastutuse jagamist? Kuidas toimivad sinodaalsed
  kogud konkreetse Kiriku tasandil? Kas need on tulemuslikud kogemused?


**9. ÄRATUNDMINE JA OTSUSTAMINE**

*Sinodaalses laadis tehakse otsuseid arusaamise abil, mis põhineb konsensusel,
mis lähtub ühisest kuulekusest Vaimule.*

Milliste protseduuride ja meetoditega me koos äratundmisele jõuame ja otsuseid
teeme? Kuidas saab neid parandada? Kuidas edendame otsuste tegemisel osalemist
hierarhiliselt struktureeritud kogukondades? Kuidas me liigendame konsultatiivse
etapi aruteluga, otsuse tegemise protsessi otsuse vastuvõtmise hetkega? Kuidas
ja milliste vahenditega edendame läbipaistvust ja aruandlust?

.. Helve:

  - eristamine ja otsustamine -- Sinodaalselt toimides tehakse otsuseid eristamise
    kaudu, tuginedes konsensusele, mis lähtub ühisest kuulekusest Vaimule.

    Milliste protseduuride ja meetoditega eristame koos ja teeme otsuseid? Kuidas
    saaks neid täiendada? Kuidas me soodustame otsustamises osalemist
    hierarhiliselt korraldatud kogukondades? Kuidas me ühendame omavahel
    konsultatiivse etapi ja kaalutlemisetapi, otsuse tegemise protsessi ja otsuse
    tegemise hetke? Kuidas ja milliste vahenditega soodustame läbipaistvust ja
    vastutavust?



**10.\  KUJUNEMINE SINODAALSUSES**

*Ühise teekonna vaimsusest on kutsutud kujunema kasvatuslik põhimõte inimese ja
kristlase, perekondade ja kogukondade kujunemisel.*

Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
vastutusrollid, et muuta nad võimekamaks „ühisel teekonnal”, üksteist kuulates
ja dialoogi pidades? Millist moodust me pakume äratundmise ja autoriteedi
rakendamiseks? Millised vahendid aitavad meil mõista meid ümbritseva kultuuri
dünaamikat ja selle mõju meie kirikustiilile?

.. Helve:
  - täienda ja kasvata -- koos rändamise vaimsus on kutsutud saama inimisiku ja kristlase, perekondade ja kogukondade kujundamise kasvatuslikuks põhimõtteks.

    Kuidas me kujundame inimesi, eriti neid, kellel on kristlikus kogukonnas
    vastutav roll, et nad oleksid veelgi suutelisemad "koos rändama", üksteist
    kuulates ja dialoogi pidades? Millist kujundamist pakume eristamise
    hõlbustamiseks ja sinodaalselt valitsemiseks? Millised vahendid aitavad meil
    mõista ümbritsevas kultuuris toimivaid jõude ja nende mõju meie laadi
    Kirikule?


Konsultatsioonile kaasaaitamiseks
---------------------------------

(31) Sinodaalse teekonna esimese etapi eesmärk on edendada laiapõhjalist
konsultatsiooniprotsessi, et koguda sinodaalselt elatud kogemuste rikkust selle
erinevates liigendustes ja tahkudes, kaasates konkreetsete kirikute pastorid ja
usklikud kõigil erinevatel tasanditel, kasutades kõige sobivamaid vahendeid
vastavalt konkreetsele kohalikule tegelikkusele: konsultatsioon, mida
koordineerib piiskop, on suunatud „kiriku preestritele, diakonitele ja
ilmikutelest ustavatel, nii individuaalselt kui ka ühendustes, jätmata
tähelepanuta väärtuslikku panust, mida pühitsetud mehed ja naised võivad anda”
(EÜ, nr 7). Konkreetsete kirikute osalusorganite panust palutakse eriti, just
presbüteraalse nõukogu ja pastoraalnõukogu oma, millest „sinodaalne kirik [võib
tõesti] hakata kuju võtma” [22]. Sama väärtuslik on ka teiste kirikuüksuste
panus, kellele ettevalmistav dokument saadetakse, samuti nende oma, kes soovivad
otse oma panuse saata. Lõpuks on ülioluline, et ka vaeste ja tõrjutute hääl
leiaks koha, mitte ainult nende oma, kellel on konkreetsetes kirikutes mingi
roll või vastutus.


(32) Süntees, mille iga konkreetne kirik selle kuulamis- ja äratundmistöö lõpus
välja töötab, on tema panus universaalse Kiriku teekonda. Teekonna järgmiste
etappide lihtsustamiseks ja jätkusuutlikumaks muutmiseks on oluline palve ja
järelemõtlemise viljad kokku suruda maksimaalselt kümnele lehele. Vajadusel
nende konteksti paremaks mõistmiseks ja selgitamiseks võib nende toetamiseks või
integreerimiseks lisada muid tekste. Tuletame meelde, et sinodi ja seega ka
selle konsultatsiooni eesmärk ei ole dokumentide koostamine, vaid „istutada
unistusi,  kutsuda esile prohvetiennustusi ja nägemusi, lubada lootusel õitseda,
usaldust äratada, haavu siduda, suhteid kokku põimida, lootuse koitu äratada,
üksteiselt õppida ja luua särav leidlikkus, mis valgustab meeli, soojendab
südant ja annab meie kätele jõudu. ”[23]
