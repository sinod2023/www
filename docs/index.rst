.. include:: ../docs/include/images.rst

.. |br| raw:: html

   <br />

.. .. raw:: html

  <table width="100%" border=0 style="background-color:black; font-size:2vw;"><tr valign="middle">
  <td width="80%" style="padding:5pt;">
  <p align="center"><font color="white">
  <b>Sinodaalse Kiriku eest</b>
  <br/>osadus | osalemine | missioon
  </font>
  </p>
  </td>
  <td width="20%" style=" padding:5pt;">
  <p style="text-align:center; font-size:1vw; font-color:white;">
  <a href="https://www.synod.va" target="blank">
  <image src="/_static/logo-2023-trasparente-et.png" width="100%"/>
  <br>www.synod.va
  </a>
  </p></td>
  </tr></table>


.. .. image:: ../docs/.static/logo-2023-trasparente-en.png
  :height: 6em
  :align: right

.. Eesti Katoliku Kiriku ametlik projekti veebileht
  **Sinodaalse Kiriku eest:**
  |br| **osadus | osalemine | missioon**
  |br|
  |br| (`English <./en/index.html>`__ | `Russian <./ru/index.html>`__)


.. panels::
  :card: col-9 border-0

  Mida Eesti rahvas kavatseb öelda XVI piiskoppide sinodil 2023. aastal Roomas?

  Siin valmib meie vastus.

  Kutsume sind :doc:`osalema <2>`.

  ---
  :card: col-3 border-0

  .. image:: /../docs/.static/LOGO_EST-web.png
    :width: 90%




.. Welcome to the **sinod.katoliku.ee** website, where we inform you about what's
  going on in Estonia regarding the :doc:`Synod on Synodality <synod>`.

  |bishop|

  .. link-button:: https://katoliku.ee/index.php/et/uudised/eesti/657-piiskopi-laekitus-eestimaa-katoliiklastele
    :classes: stretched-link
    :text: Piiskopi läkitus



.. panels::
  :card: card text-center intro-card shadow
  :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2

  |bishop|

  .. link-button:: /welcome
    :classes: stretched-link
    :text: Piiskopi läkitus

  ---

  |what|

  .. link-button:: /1
    :classes: stretched-link
    :text: Sinodist

  ---

  |listen|

  .. link-button:: /2
    :classes: stretched-link
    :text: Kuidas osaleda

  ---

  |hands|

  .. link-button:: /adsumus
    :classes: stretched-link
    :text: Sinodi palve

  ---

  |call|

  .. link-button:: /3
    :classes: stretched-link
    :text: Sinodaalsusest



Sinodaalsuse sinod Eestis
=========================

.. Sitemap

.. toctree::
    :maxdepth: 1

    welcome
    1
    2
    adsumus
    3
    links
    more
    contact

..
  Viimased uudised
  ========================

  .. blogger_latest::

  Vaata kõiki blogipostitusi: :doc:`/blog/2021/index`.
