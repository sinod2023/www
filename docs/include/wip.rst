..
  .. only:: et

    .. note::

      See lehekülg ei ole valmis ega kajasta veel sinoditiimi ühist arvamust. Sinu
      tagasiside selle lehe kohta võib meid aidata leida konsensuse.

  .. only:: en

    .. note::

      This page is work in progress and does not yet reflect the opinion of the
      core team. Your feedback about this page may help us find a consensus.

  .. only:: ru

    .. note::

      This page is work in progress and does not yet reflect the opinion of the
      core team. Your feedback about this page may help us find a consensus.
