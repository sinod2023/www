.. |listen| image:: /../docs/.static/listen.jpg
    :alt: Listen
    :width: 150

.. |call| image:: /../docs/.static/call.jpg
    :alt: Call
    :width: 150

.. |tweet| image:: /../docs/.static/tweet.jpg
    :alt: Tweet
    :width: 150

.. |creativity| image:: /../docs/.static/creativity.jpg
    :alt: Creativity
    :width: 150

.. |what| image:: /../docs/.static/what.jpg
    :alt: What
    :width: 150

.. |hands| image:: /../docs/.static/hands.jpg
    :alt: hands
    :width: 150

.. |bishop| image:: /../docs/.static/657_143.png
    :alt: bishop
    :width: 150

.. |logo| image:: /../docs/.static/LOGO_EST-web.png
    :alt: logo
    :width: 150
