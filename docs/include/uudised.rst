- 18. oktoober 2021 (Pereraadio): `Intervjuu Luc Saffre'ga
  <https://soundcloud.com/tauno-toompuu/intervjuu-luc-saffrega?si=f198b7a812c645faab9669c2ee99ac2b>`__

- 12. oktoober 2021 (ERR): `Paavst Franciscus algatas arutelu katoliku kiriku tuleviku üle
  <https://www.err.ee/1608367347/paavst-franciscus-algatas-arutelu-katoliku-kiriku-tuleviku-ule>`__

- 11. oktoober 2021 (katoliku.ee): `Piiskopi läkitus Eestimaa katoliiklastele
  <https://katoliku.ee/index.php/et/uudised/eesti/657-piiskopi-laekitus-eestimaa-katoliiklastele>`__

- 10. oktoober 2021 (katoliku.ee): `Paavst avas Sinodi
  <https://katoliku.ee/index.php/et/uudised/vaelismaa/656-paavst-avas-sinodi>`__

- 5. oktoober 2021 (katoliku.ee): `Eesti osalus algavas Sinodis
  <https://katoliku.ee/index.php/et/uudised/eesti/653-eesti-osalus-algavas-sinodis>`__
