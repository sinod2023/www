# -*- coding: utf-8 -*-

from rstgen.sphinxconf import configure
configure(globals())

project = 'Sinod 2023'
copyright = '2021 Apostellik Administratuur Eestis'
html_title = "Sinodaalsuse sinod Eestis"

# extensions += ['sphinxcontrib.youtube']
extensions += ['rstgen.sphinxconf.blog']
extensions += ['rstgen.sphinxconf.complex_tables']
# extensions += ['lino.sphinxcontrib.actordoc']
# extensions += ['sphinxcontrib.taglist']
# extensions += ['sphinx.ext.inheritance_diagram']

# extensions += ['rstgen.sphinxconf.sigal_image']
# sigal_base_url = 'https://sigal.saffre-rumma.net'

# html_short_title = "⌘"

# html_logo = str(Path('../docs/.static/logo-2023-trasparente-et.png').resolve())
# html_logo = str(Path('../docs/.static/CAHR-people-on-the-move-400.jpg').resolve())

if html_theme == "insipid":
    html_context.update({
        'display_gitlab': True,
        'gitlab_user': 'sinod2023',
        'gitlab_repo': 'www',
    })

# html_theme_options.update(globaltoc_collapse=True)

html_context.update(
    srcref_url='https://gitlab.com/sinod2023/www/blob/master/%s')

# html_context.update(public_url="https://sinod2023.gitlab.io/www")
html_context.update(public_url="https://sinod.katoliku.ee")

templates_path.insert(0, str(Path("../docs/.templates").resolve()))
# templates_path.append(str(Path(".templates").resolve()))

language = "et"


if html_theme == "insipid":
    html_theme_options = {
        # 'body_max_width': None,
        # 'breadcrumbs': True,
        'globaltoc_includehidden': False,
        'left_buttons': [
            'search-button.html',
            'home-button.html',
        ],
        'right_buttons': [
            'fullscreen-button.html',
            'repo-button.html',
            'facebook-button.html',
        ],
    }
    html_sidebars = {
        '**' : ['languages.html',
            'globaltoc.html',
            'separator.html',
            'searchbox.html',
            'indices.html',
            'links.html'
            ]}

"""

The following is to support language specific content using the :rst:dir:`only`
directive.

"""

def on_config_inited(app, config):
    tags.add(config.language)

def setup(app):
    # print("20211021", app)
    app.connect('config-inited', on_config_inited)

# html_css_files.append('sinod-cust.css')
