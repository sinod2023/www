===========
Autoriõigus
===========

.. include:: ../endocs/copyright.rst
  :start-line: 3

..
  Selle saidi väljaandja on Apostellik Administratuur Eestis.

  | Apostellik Administratuur Eestis
  | Vene 18
  | 10123 Tallinn
  | https://www.katoliku.ee

  .. raw:: html

    <p xmlns:cc="http://creativecommons.org/ns#" ><a rel="cc:attributionURL"
    href="https://sinod.laudate.ee">This work</a> by <a rel="cc:attributionURL
    dct:creator" property="cc:attributionName"
    href="https://www.katoliku.ee">Apostellik Administratuur Eestis</a> is
    licensed under <a
    href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1"
    target="_blank" rel="license noopener noreferrer"
    style="display:inline-block;">CC BY 4.0<img
    style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
    src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
    style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
    src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>
