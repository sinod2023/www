============
Sinodi palve
============

.. image:: /../docs/.static/hands.jpg
  :width: 30%
  :align: right

..  Kui oleme koos sinodaalsel teekonnal, järgmine palve kutsub Püha Vaimu meis
  tegutsema, et oleksime kogukond ja armu rahvas. Sinodi 2021-2023 jaoks pakume
  välja järgmise  See on lihtsustatud variant *Adsumus Sancte Spiritus* palvest,
  millega algas iga II Vatikani Kirikukogu istung.

Palugem Püha Vaimu abi, et suudaksime elada ja tegutseda kogukonnana ja Jumala
armu rahvana.  Siinkohal toodud sinodi palve on kohandatud sõnastus palvest
*Adsumus Sancte Spiritus*, millega algas iga Vatikani II Kirikukogu istung.

  | Sinu ees seisame, Püha Vaim,
  | Sinu nimel oleme kokku tulnud.
  | Meie ainus nõuandja, Sind palume:
  | tule meie juurde,
  | rutta meile appi,
  | võta aset meie südameis.
  | Õpeta meile, kuidas leida teed;
  | näita meile, kuidas jõuda sihini.
  | Me oleme nõrgad ja patused;
  | ära luba meil segadust külvata.
  | Ära luba meil teadmatusest eksiteele sattuda
  | ega eelarvamustel meie tegusid juhtida.
  | Luba meil leida Sinus ühtsus,
  | nii et rändaksime koos igavese elu poole,
  | püsides tõeteel ja selles, mis on õige.
  | Kõike seda palume Sinult,
  | kes sa tegutsed kõigis paigus igal ajal
  | üheskoos Isa ja Pojaga
  | nüüd ja igavesti.
  | Aamen.


Tõlge alused:
https://www.synod.va/en/documents/adsumus.html
(EN ja DE versioonid) ja arutelu sinoditiimis.
