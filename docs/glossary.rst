========
Glossary
========

.. glossary::

  Sensus fidei

    A universal consent in matters of faith and morals, manifested by the whole
    people of God, from the bishops to the last of the faithful.

    https://en.wikipedia.org/wiki/Sensus_fidelium

  ITC

    International Theological Commission

    https://en.wikipedia.org/wiki/International_Theological_Commission

  CDF

    Congregation for the Doctrine of the Faith

    https://en.wikipedia.org/wiki/Congregation_for_the_Doctrine_of_the_Faith
