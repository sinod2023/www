================
Piiskopi läkitus
================

.. image:: /../docs/.static/657_143.png
  :width: 30%
  :align: right


Armsad vennad ja õed!

„Jeesus vastas Peetrusele: „Mina ütlen sulle: sina oled Peetrus ja sellele
kaljule ma ehitan oma kiriku ning põrguväravad ei saa sellest võitu.““ (Mt
16:17) Need Jeesuse sõnad on suur lubadus kogu inimkonnale ja kõikidele
kristlastele. Jeesus, mitte inimesed, on see, kes ehitab Kirikut, ning see Kirik
püsib oma Lunastaja õpetuses vaatamata ajaloosündmustele ja inimese patule.
Peetrus – ja tema järglased – on Rooma piiskopina, tõelise kaljuna, millele
toetub Jeesus, Kiriku ja Kiriku usu ühtsuse märgiks ja aluseks.

Loomulikult ei tähenda need sõnad, et ainult Rooma piiskop juhib ja õpetab
Kristuse Kirikut. Seetõttu on Kiriku algusaastatest peale toimunud palju
kirikukogusid ning sinodeid –  suuremaid või väiksemaid piiskoppide kohtumisi,
mille eesmärk on arutada, osaduses Rooma paavstiga, Kiriku õpetust või elu
puudutavaid küsimusi, neid süvendada Pühakirja ja Kiriku Pärimuse alusel ja tuua
need uuesti üha suurema selguse ja konkreetsusega kõikide katoliiklasteni.
Pärast II Vatikani kirikukogu väljendub see traditsioon korralistes piiskoppide
sinodites. Need on piiskoppide kohtumised, mis leiavad aset umbes kahe aasta
tagant, et arutada paavsti palvel Kiriku elu puudutavaid küsimusi. Järgmine
piiskoppide sinod on 16. korraline sinod ja see toimub 2023. aasta oktoobris.

Tuleva piiskoppide sinodi teema on Kiriku sinodaalsus: millisel viisil võiks
kogu Jumala rahvas, kõik katoliiklased ja igaüks meist aktiivselt osaleda Kiriku
elus ja sellega seonduvates otsustes. Sel korral palus Rooma paavst Franciscus,
et enne sinodi kokku tulemist toimuks igal pool maailmas katoliiklaste
nõupidamine. See nõupidaminepeaks olema nii laiahaardeline kui võimalik. Paavsti
eesmärk on koostada sinodi nn instrumentum laboris ehk „töövahend“ nõupidamisel
kogutud ettepanekutega, mida piiskoppide sinodil osalejad 2023. aastal arutama
hakkavad. Sinodi esimene faas algab kõikides piiskopkondades üle maailma
piduliku Missaga pühapäeval, 17. oktoobril 2021.

Armsad vennad ja õed, ma palun teil vastata suuremeelselt paavsti palvele sellel
nõupidamisel osaleda. See on märk armastusest Kiriku vastu ja soovist osaleda
tema kui Kristuse Ihu elus. Ja isegi, kui me oleme osa Kristuse Ihust eelkõige
sakramentide kaudu, tugevdab Kristuse Kiriku – mida Kristus pidevalt ehitab –
elus osalemine meie kristlikku kutsumust. Püha Tool on avaldanud vajalikud
juhised ja teemapunktid, mille üle paavst Franciscus palub arutada, ning mille
tõlked saavad peagi valmis. Meilt oodatakse mitte ainult küsimustiku täitmist,
vaid seda, et meie kogudustes, palvegruppides, piibliringides ja mujal tekiks
elav arutelu ja mõttevahetus, sest me moodustame ühe pere, milleks on Kirik.

Ma tänan väga kõiki neid, kes tahavad selles arutelus osaleda, sealhulgas eriti
neid, kes on valmis kaasa aitama, et see arutelu toimuks Eestis nii sujuvalt ja
organiseeritult kui võimalik. Püha Tooli soovituste kohaselt olen nimetanud sel
puhul meedia referendi, kelleks on pr Marge-Marie Paas, ja sinodi referendi hr
Luc Saffre’i – mõlemad Tallinna Peeter-Pauli kogudusest. Teised erinevate
koguduste liikmed aitavad neid, et vajalik info jõuaks iga katoliiklase või
huviliseni. Palun aidake neid, et nende töö oleks kergem ja viljakas.

Need kolm suurt teemat, millesse paavst Franciscus palub meil süveneda, on
„communion, participation, mission“ (osadus, osalemine, missioon). Need ei ole
abstraktsed teemad ega teoreetilised küsimused, vaid ühinevad ja koonduvad kokku
üheks: misjon –  kuidas saab Katoliku Kirik meie linnas, Eestis, Euroopas ja
maailmas paremini täita missiooni, mille selle Asutaja on talle usaldanud:
„Minge kõike maailma, kuulutage evangeeliumi kogu loodule!“ (Mk16:15) Ja kuidas
igaüks meist võib selles missioonis aktiivsemalt osaleda.

Nüüd, oktoobrikuus, mis on traditsiooniliselt roosipärjakuu, pöördugem eriti
suure usaldusega Jumalaema eestkostele Roosipärja kaudu, et see sinod ning töö,
mis sellele kahe aasta jooksul eelneb, kannaks väga palju vilja Jumala auks ja
inimeste heaks.



\+ Piiskop Philippe Jourdan

Tallinnas, 10.10.2021

(Vaata ka `originaal
<https://katoliku.ee/index.php/et/uudised/eesti/657-piiskopi-laekitus-eestimaa-katoliiklastele>`__)
