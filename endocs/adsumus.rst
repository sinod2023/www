======
Prayer
======

.. image:: /../docs/.static/hands.jpg
  :width: 30%
  :align: right



As we are called to embrace this synodal path of the Synod 2021-2023, the
following prayer invites the Holy Spirit to operate within us so that we may be
a community and a people of grace. It is a simplified version of the *Adsumus
Sancte Spiritus*, which was used at every session of the Second Vatican
Council.

  | We stand before You, Holy Spirit,
  | as we gather together in Your name.
  | With You alone to guide us,
  | make Yourself at home in our hearts;
  | Teach us the way we must go
  | and how we are to pursue it.
  | We are weak and sinful;
  | do not let us promote disorder.
  | Do not let ignorance lead us down the wrong path
  | nor partiality influence our actions.
  | Let us find in You our unity
  | so that we may journey together to eternal life
  | and not stray from the way of truth
  | and what is right.
  | All this we ask of You,
  | who are at work in every place and time,
  | in the communion of the Father and the Son,
  | forever and ever.
  | Amen.
