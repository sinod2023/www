.. include:: ../docs/include/images.rst

.. |br| raw:: html

   <br />


.. .. raw:: html

  <table width="100%" border=0 style="background-color:black; font-size:2vw"><tr valign="middle">
  <td width="70%" style="padding:5pt;">
  <p align="center"><font color="white">
  <b>For a synodal Church</b>
  <br/>communion | participation | mission
  </font>
  </p>
  </td>
  <td width="30%" style=" padding:5pt;">
  <p style="text-align:center; font-size:1vw; font-color:white;">
  <a href="https://www.synod.va" target="blank">
  <image src="/_static/logo-2023-trasparente-en.png" width="100%"/>
  <br>www.synod.va
  </a>
  </p></td>
  </tr></table>



.. .. raw:: html
  <a href="https://www.synod.va" target="_blank"><image src="/_static/logo-2023-trasparente-en.png" width="30%" align="right"/ style="background:black; padding:5pt;"></a>

.. .. image:: ../docs/.static/logo-2023-trasparente-en.png
  :height: 6em
  :align: right

.. Official project website of for the Synod on Synodality:

.. Estonian Catholic Church
  For a synodal Church:
  |br| communion | participation | mission
  |br|
  |br| (also in `Estonian <../index.html>`__  and `Russian <../ru/index.html>`__)


.. Welcome to the **sinod.katoliku.ee** website, where we inform you about what's
  going on in Estonia regarding the :doc:`Synod on Synodality <synod>`.


.. panels::
  :card: col-9 border-0

  What is Estonia going to say in October 2023 at the 16th Synod of Bishops in
  Rome?

  This website prepares our answer.

  We invite you to :doc:`participate <2>`.

  ---
  :card: col-3 border-0

  .. image:: /../docs/.static/LOGO_ENG-web.jpg
    :width: 90%




.. panels::
  :card: card text-center intro-card shadow
  :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2

  |bishop|

  .. link-button:: /en/welcome
    :classes: stretched-link
    :text: Bishop's message

  ---

  |what|

  .. link-button:: /en/1
    :classes: stretched-link
    :text: About the Synod

  ---

  |listen|

  .. link-button:: /en/2
    :classes: stretched-link
    :text: How to participate

  ---

  |hands|

  .. link-button:: /en/adsumus
    :classes: stretched-link
    :text: Prayer

  ---

  |call|

  .. link-button:: /en/3
    :classes: stretched-link
    :text: About synodality



Synod on Synodality Estonia
===========================

.. Sitemap

.. toctree::
    :maxdepth: 1

    Bishop's message <welcome>
    1
    2
    adsumus
    3
    links
    more
    contact


..
  Latest blog entries
  ===================

  .. blogger_latest::

  See all blog entries in :doc:`/blog/2021/index`.
