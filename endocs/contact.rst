=======
Contact
=======

Please submit your meeting reports, comments, questions or feedback to the
following address:

  sinod@katoliku.ee

Your email will be sent to the **core team**, which consists of six persons: Luc
Saffre (synodal referent), Marge Paas (media referent), father Edgaras
Versockis, Helle Helena Puusepp, Helve Trumann and Johanna Aus.

The core team acts in a synodal way under the authority of Mgr Bishop Philippe
Jourdan who appoints its members.

If you want to join our team or contribute to the Synod in a special way, please
contact Luc Saffre.

.. référent = contact person = sinodi referent
.. référent = contact person = meedia referent

..
  The core team acts in a synodal way under the authority of Mgr Bishop Philippe
  Jourdan who appointed its members.  The Bishop alone can revoke a team member or
  call new individuals to join the team.

  We decide about all questions related to the Synod on Synodality in Estonia. Our
  job is to run the consultation phase in Estonia and then to synthesize the
  results into a report.  This website is one result of our common work.
  We gather in regular meetings and communicate among each other in order to
  synchronize our individual contributions to the project.


  We decide about all questions related to the Synod on Synodality in Estonia. Our
  job is to run the consultation phase in Estonia and then to synthesize the
  results into a report.  This website is one result of our common work.


  The Synod of Bishops gives guidelines about what they expect from us in two
  documents of some 70 pages (see :doc:`links`).

.. See especially :term:`Vademecum` (4.4 2.) and :term:`SynDCP`.


.. The Vatican expects us to work in a "synodal" way. Most of us hadn't even heard
  that word when we started.  Actually we all are still discovering what
  "synodality" means.
