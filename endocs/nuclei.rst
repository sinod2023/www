=========================
10 themes for inspiration
=========================

To help you explore the :ref:`fundamental question <question>` more fully, the
following themes highlight significant aspects of lived synodal­ity and can be
used as a starting point for exchange and dialogue.  Your exchange and dialogue
do not need to be limited to these themes. (:term:`PD`, 30)

**1.  COMPANIONS ON THE JOURNEY**

*In the Church and in society we are side by side on the same road.*

In our local Church, who are those who “walk together”? Who are those who seem
further apart? How are we called to grow as companions? What groups or
individuals are left on the margins?

**2. LISTENING**

*Listening is the first step, but it requires an open mind and heart, without
prejudice.*

How is God speaking to us through voices we sometimes ignore? How are the laity
listened to, especially women and young people? What facilitates or inhibits our
listening? How well do we listen to those on the peripheries? How is the
contribution of consecrated men and women integrated? What are some limitations
in our ability to listen, especially to those who have different views than our
own? What space is there for the voice of minorities, especially people who
experience poverty, marginalization, or social exclusion?

**3. SPEAKING OUT**

*All are invited to speak with courage and parrhesia, that is, in freedom, truth, and
charity.*

What enables or hinders speaking up courageously, candidly, and responsibly in
our local Church and in society? When and how do we manage to say what is
important to us? How does the relationship with the local media work (not only
Catholic media)? Who speaks on behalf of the Christian community, and how are
they chosen?

**4. CELEBRATION**

*“Walking together” is only possible if it is based on communal listening to the
Word and the celebration of the Eucharist.*

How do prayer and liturgical celebrations actually inspire and guide our common
life and mission in our community? How do they inspire the most important
decisions? How do we promote the active participation of all the faithful in the
liturgy? What space is given to participating in the ministries of lector and
acolyte?

**5. SHARING RESPONSIBILITY FOR OUR COMMON MISSION**

*Synodality is at the service of the mission of the Church, in which all members
are called to participate.*

Since we are all missionary disciples, how is every baptised person called to
participate in the mission of the Church? What hinders the baptised from being
active in mission? What areas of mission are we neglecting? How does the
community support its members who serve society in various ways (social and
political involvement, scientific research, education, promoting social justice,
protecting human rights, caring for the environment, etc.)? How does the Church
help these members to live out their service to society in a missionary way? How
is discernment about missionary choices made and by whom?

**6. DIALOGUE IN CHURCH AND SOCIETY**

*Dialogue requires perseverance and patience, but it also enables mutual
understanding.*

To what extent do diverse peoples in our community come together for dialogue?
What are the places and means of dialogue within our local Church? How do we
promote collaboration with neighbouring dioceses, religious communities in the
area, lay associations and movements, etc.? How are divergences of vision, or
conflicts and difficulties addressed? What particular issues in the Church and
society do we need to pay more attention to? What experiences of dialogue and
collaboration do we have with believers of other religions and with those who
have no religious affiliation? How does the Church dialogue with and learn from
other sectors of society: the spheres of politics, economics, culture, civil
society, and people who live in poverty?

**7. ECUMENISM**

*The dialogue between Christians of different confessions, united by one baptism,
has a special place in the synodal journey.*

What relationships does our Church community have with members of other
Christian traditions and denominations? What do we share and how do we journey
together? What fruits have we drawn from walking together? What are the
difficulties? How can we take the next step in walking forward with each other?

**8. AUTHORITY AND PARTICIPATION**

*A synodal church is a participatory and co-responsible Church.*

How does our Church community identify the goals to be pursued, the way to reach
them, and the steps to be taken? How is authority or governance exercised within
our local Church? How are teamwork and co-responsibility put into practice? How
are evaluations conducted and by whom? How are lay ministries and the
responsibility of lay people promoted? Have we had fruitful experiences of
synodality on a local level? How do synodal bodies function at the level of the
local Church (Pastoral Councils in parishes and dioceses, Presbyteral Council,
etc.)? How can we foster a more synodal approach in our participation and
leadership?

**9. DISCERNING AND DECIDING**

*In a synodal style we make decisions through discernment of what the Holy Spirit
is saying through our whole community.*

What methods and processes do we use in decision-making? How can they be
improved? How do we promote participation in decision-making within hierarchical
structures? Do our decision- making methods help us to listen to the whole
People of God? What is the relationship between consultation and
decision-making, and how do we put these into practice? What tools and
procedures do we use to promote transparency and accountability? How can we grow
in communal spiritual discernment?

**10.  FORMING OURSELVES IN SYNODALITY**

*Synodality entails receptivity to change, formation, and on-going learning.*

How does our church community form people to be more capable of “walking
together,” listening to one another, participating in mission, and engaging in
dialogue? What formation is offered to foster discernment and the exercise of
authority in a synodal way?

(Source: :term:`Vademecum` section 5.3 and :term:`PD` item 30)
