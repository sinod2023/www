================
About synodality
================

.. include:: ../docs/include/wip.rst


.. image:: /../docs/.static/call.jpg
  :width: 40%
  :align: right


.. contents::
  :local:


The Church is synodal
=====================

The Catholic Church is organized in a way (*modus vivendi et operando*) that is
neither democratic, nor monarchistic, nor anarchistic. It seems that the best
word for this model is "synodal". The Church herself is only beginning to
discover this term.

  "Synodality is the way of being the Church today according to the will of God,
  in a dynamic of discerning and listening together to the voice of the Holy
  Spirit." -- Pope Francis

.. _neologism:

A neologism
===========

What does "The church is synodal" mean? I know that a synod is some kind of
Church council where high-level clericals decide about high-level issues. But
don't tell me that Church lives only in some assemblies of bishops!

What does "synodality" mean here?

The word "synodal" comes from greek  *συν* (together) and *ὁδός* (journey).
So it has the same roots as the word "synod".
But actually it is an independent sibling of their common parents, a neologism.
It appeared in the Catholic Church only recently.

Why did this word appear and what does it mean?

Somewhere around the millenium people started to ask: How does the Church
function? The organization model (*modus vivendi et operando*) of the Church is
obviously neither democratic nor monarchistic.

A comprehensive answer to this question is being demanded with more and more urgency.
Not only because hefty internal debates create a need for clear answers to certain faith questions.
Not only because public order demands explanations about administrative rules
and suggests optimizations for these rules.
There is another, even more important reason:
humankind as a whole struggles with exactly the same kind of challenges as the Church.
The Church obviously has much experience in organizing our living together, and this know-how
might be useful to public administrations and governments as well.

The Church herself started only quite recently to discover that her organization
model has something special about it and that it deserves a name.  The word "synodality",
emerged as a name for this model.

  "What the Lord is asking of us is already in some sense present in the very
  word "Synod." Journeying together – laity, pastors, the Bishop of Rome – is an
  easy concept to put into words, but not so easy to put into practice."
  -- Pope Francis

So we can define *Synodality* as "our way of living together". That's nice, but
of course we need to explain this in more detail.

The Church has done already some work for explaining synodality.
A number of documents has been published by the Vatican.
See the "Resources" section on our website.
Pope Francis used it during a speech on :term:`20151017`.
The first explicit document is
`Synodality in the life and mission of the Church.
<https://www.vatican.va/roman_curia/congregations/cfaith/cti_documents/rc_cti_20180302_sinodalita_en.html>`__,
a study lead by the :term:`ITC` and published in March 2018.

In 2019, German Catholics started a national project they called the "Synodal
Path". In 2021, some of them were disappointed when Francis asked them to "Calm
down, these issues aren't limited to Germany, so these discussions must be led
by the Vatican". One can say that the Synod on Synodality is a genial answer to
and continuation of the Synodal Path.

.. https://www.vaticannews.va/de/kirche/news/2021-10/deutschland-synodaler-weg-endet-vorzeitig-mangelnder-teilnehmer.html
.. https://www.vaticannews.va/de/vatikan/news/2021-10/vatikan-synode-kirchengeschichte-dokumente-papst-entwicklung-faq.html


Attitudes in a synodal process
================================

(Source: :term:`Vademecum`, section 2.3)

On various occasions, Pope Francis has shared his vision for what the practice
of synodality looks like concretely. The following are particular attitudes that
enable genuine listening and dialogue as we participate in the Synodal Process.

- Being synodal requires time for sharing: We are invited to speak with authentic
  courage and honesty (parrhesia) in order to integrate freedom, truth, and charity.
  Everyone can grow in understanding through dialogue.

- Humility in listening must correspond to courage in speaking: Everyone has the
  right to be heard, just as everyone has the right to speak. Synodal dialogue depends
  on courage both in speaking and in listening. It is not about engaging in a debate to
  convince others. Rather, it is welcoming what others say as a way by which the Holy
  Spirit can speak for the good of all (1 Corinthians 12:7).

- Dialogue leads us to newness: We must be willing to change our opinions based
  on what we have heard from others.

- Openness to conversion and change: We can often be resistant to what the Holy
  Spirit is trying to inspire us to undertake. We are called to abandon attitudes of
  complacency and comfort that lead us to make decisions purely on the basis of how
  things have been done in the past.

- Synods are an ecclesial exercise in discernment: Discernment is based on the
  conviction that God is at work in the world and we are called to listen to what the
  Spirit suggests to us.

- We are signs of a Church that listens and journeys: By listening, the Church
  follows the example of God himself, who listens to the cry of his people. The Synodal
  Process provides us with the opportunity to open ourselves to listen in an authentic
  way, without resorting to ready-made answers or pre-formulated judgments.

- Leave behind prejudices and stereotypes: We can be weighed down by our
  weaknesses and sinfulness. The first step towards listening is freeing our minds and
  hearts from prejudices and stereotypes that lead us on the wrong path, towards
  ignorance and division.

- Overcome the scourge of clericalism: The Church is the Body of Christ filled
  with different charisms in which each member has a unique role to play. We are
  all interdependent on one another and we all share an equal dignity amidst the
  holy People of God. In the image of Christ, true power is service. Synodality
  calls upon pastors to listen attentively to the flock entrusted to their care,
  just as it calls the laity to freely and honestly express their views.
  Everyone listens to one other out of love, in a spirit of communion and our
  common mission. Thus the power of the Holy Spirit is manifested in manifold
  ways in and through the entire People of God.

- Cure the virus of self-sufficiency: We are all in the same boat. Together we
  form the Body of Christ. Setting aside the mirage of self-sufficiency, we are
  able to learn from each other, journey together, and be at the service of one
  another. We can build bridges beyond the walls that sometimes threaten to
  separate us – age, gender, wealth, ability, education, etc.

- Overcoming ideologies: We must avoid the risk of giving greater importance to
  ideas than to the reality of the life of faith that people live in a concrete way.
- Give rise to hope: Doing what is right and true does not seek to attract attention or
  make headlines, but rather aims at being faithful to God and serving His People. We
  are called to be beacons of hope, not prophets of doom.

- Synods are a time to dream and "spend time with the future": We are encouraged
  to create a local process that inspires people, with no one excluded to create
  a vision of the future filled with the joy of the Gospel. The following
  dispositions will help participants.


Dispositions in a synodal process
=================================

(Source: :term:`Vademecum`, section 2.3)

The following dispositions will help participants to create a local process that
inspires people, with no one excluded to create a vision of the future filled
with the joy of the Gospel:

- An innovative outlook: To develop new approaches, with creativity and a
  certain audacity.

- Being inclusive: A participatory and co-responsible Church, capable of
  appreciating its own rich variety, embraces all those we often forget or ignore.

- An open mind: Let us avoid ideological labels and make use of all
  methodologies that have borne fruit.

- Listening to each and every one: By learning from one another, we can
  better reflect the wonderful multi-faceted reality that Christ’s Church is meant
  to be.

- An understanding of “journeying together”: To walk the path that God
  calls the Church to undertake for the third millennium.

- Understanding the concept of a co-responsible Church: To value and
  involve the unique role and vocation of each member of the Body of Christ,
  for the renewal and building up of the whole Church.

- Reaching out through ecumenical and interreligious dialogue: To dream
  together and journey with one another throughout the entire human family.


Avoiding pitfalls
=================

(Source: :term:`Vademecum`, section 2.4)

As on any journey, we need to be aware of possible pitfalls that could hamper
our progress during this time of synodality. The following are several pitfalls
that must be avoided in order to promote the vitality and fruitfulness of the
Synodal Process.

- The temptation of wanting to lead ourselves instead of being led by God.
  Synodality is not a corporate strategic exercise. Rather it is a spiritual
  process that is led by the Holy Spirit. We can be tempted to forget that we
  are pilgrims and servants on the path marked out for us by God. Our humble
  efforts of organization and coordination are at the service of God who guides
  us on our way. We are clay in the hands of the divine Potter (Isaiah 64:8).

- The temptation to focus on ourselves and our immediate concerns. The Synodal
  Process is an opportunity to open up, to look around us, to see things from
  other points of view, and to move out in missionary outreach to the
  peripheries. This requires us to think long-term. This also means broadening
  our perspectives to the dimensions of the entire Church and asking questions,
  such as: What is God’s plan for the Church here and now? How can we implement
  God’s dream for the Church on the local level?

- The temptation to only see “problems.” The challenges, difficulties, and
  hardships facing our world and our Church are many. Nevertheless, fixating on
  the problems will only lead us to be overwhelmed, discouraged, and cynical. We
  can miss the light if we focus only on the darkness. Instead of focusing only
  on what is not going well, let us appreciate where the Holy Spirit is
  generating life and see how we can let God work more fully.

- The temptation of focusing only on structures. The Synodal Process will
  naturally call for a renewal of structures at various levels of the Church, in
  order to foster deeper communion, fuller participation, and more fruitful
  mission. At the same time, the experience of synodality should not focus first
  and foremost on structures, but on the experience of journeying together to
  discerning the path forward, inspired by the Holy Spirit. The conversion and
  renewal of structures will come about only through the on-going conversion and
  renewal of all the members of the Body of Christ.

- The temptation not to look beyond the visible confines of the Church. In
  expressing the Gospel in our lives, lay women and men act as a leaven in the
  world in which we live and work. A Synodal Process is a time to dialogue with
  people from the worlds of economics and science, politics and culture, arts
  and sport, the media and social initiatives. It will be a time to reflect on
  ecology and peace, life issues and migration. We must keep the bigger picture
  in view to fulfil our mission in the world. It is also an opportunity to
  deepen the ecumenical journey with other Christian denominations and to deepen
  our understanding with other faith traditions.

- The temptation to lose focus of the objectives of the Synodal Process. As we
  proceed along the journey of the Synod, we need to be careful that, while our
  discussions might be wide-ranging, the Synodal Process maintains the goal of
  discerning how God calls us to walk forward together. No one Synodal Process
  is going to resolve all our concerns and problems. Synodality is an attitude
  and an approach of moving forward in a co-responsible way that is open to
  welcoming God’s fruits together over time.

- The temptation of conflict and division. “That they may all be one” (John
  17:21). This is the ardent prayer of Jesus to the Father, asking for unity
  among his disciples. The Holy Spirit leads us deeper into communion with God
  and one another. The seeds of division bear no fruit. It is vain to try to
  impose one’s ideas on the whole Body through pressure or to discredit those
  who feel differently.

- The temptation to treat the Synod as a kind of a parliament. This confuses
  synodality with a ‘political battle’ in which in order to govern one side must
  defeat the other. It is contrary to the spirit of synodality to antagonize
  others or to encourage divisive conflicts that threaten the unity and
  communion of the Church,

- The temptation to listen only to those who are already involved in Church
  activities. This approach may be easier to manage, but it ultimately ignores a
  significant proportion of the People of God.
