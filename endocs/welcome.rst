=======================================
Bishop’s message to Estonia’s Catholics
=======================================

.. image:: /../docs/.static/657_143.png
  :width: 30%
  :align: right


Dear brothers and sisters!

"And I say to thee: That thou art Peter; and upon this rock I will build my
church, and the gates of hell shall not prevail against it.” (Mt. 16:18). These
words of Jesus are a great promise to all of mankind and to all Christians.
Jesus, and not the people, is the one who builds the Church, and this Church
remains in the teachings of her Redeemer, in spite of historical events and the
sin of mankind. Peter – and his descendants – as bishop of Rome, the true rock
which Jesus supports, is the sign and foundation of the unity of the Church, and
the Church's faith.



Naturally, these words do not mean that it is only the Bishop of Rome who leads
and teaches Christ’s Church. There have thus, since the early years of the
Church, been many Church congregations and synods – larger or smaller assembles
of Bishops which discuss, in communion with the Pope of Rome, questions of
church teaching or life, deepening these as based on scripture and Church
tradition, and bringing these anew, with an increased clarity and specificity,
to all Catholics. After the Vatican Second Council, this tradition has been
expressed in the regular Bishops’ Synods. These are assemblies of Bishops which
take place around every two years, at the request of the Pope, to discuss
matters concerning the life of the Church. The next Synod of Bishops will be the
16th Ordinary General Assembly of the Synod of Bishops, which will take place in
October 2023.

The theme of the coming Synod of Bishops is the synodality of the Church: In
which ways could God’s people in their entirety, all Catholics and every one of
us, be actively involved in the life of the Church and in the decisions which
arise from that. On this occasion, Pope Francis of Rome has called for the
consultation of Catholics worldwide, ahead of the Synod convening. This
consultation should be as comprehensive as is possible. The Pope’s goal is to
compose the Synods so-entitled Instrumentum Laboris, or “tool”, with proposals
gathered during at the meeting to be discussed by the participants of the Synod
of Bishops in 2023. The Synod’s initial phase will commence in all dioceses
worldwide with a festive Mass, on Sunday, October 17, 2021.

Dear brothers and sisters, I ask you to respond generously to the Pope’s request
to attend this meeting. It is a sign of love for the Church, and a desire to
partake in it as the life of the Body of Christ. And even if we are primarily a
part of the Body of Christ via the sacraments, participation in the life of the
Church of Christ – which Christ is continually building – strengthens our
Christian vocation. The Holy See has published the required guidelines and
topics which Pope Francis requests be discussed, and whose translations will
soon be ready. We are expected not only to complete this questionnaire, but also
to hold a lively discussion and debate in our congregations, prayer groups,
Bible circles and so one, since we form one family, namely the Church.

I thank very much all those who desire to take part in this debate, including
those who are prepared to help facilitate this debate in Estonia to be as smooth
and as organized as possible. In accordance with the recommendations of the Holy
See, I have appointed a media rapporteur for this occasion, namely Ms.
Marge-Marje Paas, and a Synod rapporteur, Mr Luc Saffre, both from the St. Peter
and St. Paul’s Congregation, in Tallinn. Other members of the various
congregations will help them to get the necessary information to every Catholic
or interested party. Please help them to make their work easier, and more
fruitful.

The three major themes which Pope Francis requests us to delve into are: “Communion, participation and mission”. These are not abstract topics or theoretical questions, but merge and converge into one: Mission – how can the Catholic Church in our city, in Estonia, in Europe and in the world better fulfill the mission entrusted to it by its Founder: “Go ye into the whole world, and preach the gospel to every creature.” (Mk. 16:15). And how every one of us can participate more actively in this mission.

Now, in the month of October, traditionally the month of the Holy Rosary, let us return with great confidence towards the intercession of the Mother of God, through the Holy Rosary, that this Synod and the work which precedes it over the two years will bear much fruit for the glory of God and for the good of the people.



\+ Bishop Philippe Jourdan

Tallinn, 10.10.2021

(Copied from `the official document <https://katoliku.ee/index.php/en/news/658-bishop-s-message-to-estonia-s-catholics>`__)
