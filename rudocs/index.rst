.. include:: ../docs/include/images.rst

.. |br| raw:: html

   <br />


.. .. raw:: html

  <table width="100%" border=0 style="background-color:black; font-size:2vw"><tr valign="middle">
  <td width="70%" style="padding:5pt;">
  <p align="center"><font color="white">
  <b>For a synodal Church</b>
  <br/>communion | participation | mission
  </font>
  </p>
  </td>
  <td width="30%" style=" padding:5pt;">
  <p style="text-align:center; font-size:1vw; font-color:white;">
  <a href="https://www.synod.va" target="blank">
  <image src="/_static/logo-2023-trasparente-en.png" width="100%"/>
  <br>www.synod.va
  </a>
  </p></td>
  </tr></table>



.. .. raw:: html
  <a href="https://www.synod.va" target="_blank"><image src="/_static/logo-2023-trasparente-en.png" width="30%" align="right"/ style="background:black; padding:5pt;"></a>

.. .. image:: ../docs/.static/logo-2023-trasparente-en.png
  :height: 6em
  :align: right

.. Official project website of for the Synod on Synodality:

.. Estonian Catholic Church
  For a synodal Church:
  |br| communion | participation | mission
  |br|
  |br| (also in `Estonian <../index.html>`__  and `Russian <../ru/index.html>`__)


.. Welcome to the **sinod.katoliku.ee** website, where we inform you about what's
  going on in Estonia regarding the :doc:`Synod on Synodality <synod>`.


.. panels::
  :card: col-9 border-0

  Что Скажет Эстония в октябре 2023 года на 16-м Архиерейском Синоде в Риме?

  Этот сайт готовит наш ответ.

  Приглашаем Вас к :doc:`участию <2>`.

  ---
  :card: col-3 border-0

  .. image:: /../docs/.static/LOGO_RUS-web.png
    :width: 90%



.. panels::
  :card: card text-center intro-card shadow
  :column: col-lg-4 col-md-4 col-sm-6 col-xs-12 p-2

  |bishop|

  .. link-button:: /ru/welcome
    :classes: stretched-link
    :text: Послание епископа

  ---

  |what|

  .. link-button:: /ru/1
    :classes: stretched-link
    :text: Что такое Синод?

  ---

  |listen|

  .. link-button:: /ru/2
    :classes: stretched-link
    :text: Как принять участие

  ---

  |call|

  .. link-button:: /ru/3
    :classes: stretched-link
    :text: About synodality

  ---

  |hands|

  .. link-button:: /ru/adsumus
    :classes: stretched-link
    :text: Молитва



Synod on Synodality Estonia
===========================

.. Sitemap

.. toctree::
    :maxdepth: 1

    Послание епископа <welcome>
    1
    2
    Молитва <adsumus>
    3
    links
    more
    contact


..
  Latest blog entries
  ===================

  .. blogger_latest::

  See all blog entries in :doc:`/blog/2021/index`.
