=========
Copyright
=========

Pictures on this website are taken from https://www.synod.va/en/resources1.html
(SOCIAL MEDIA TOOLKIT).

All other content on this website is written and maintained
by the Synodal core team of the Catholic Church in Estonia
under authority of

| Apostellik Administratuur Eestis
| Vene 18
| 10123 Tallinn
| https://www.katoliku.ee

You may publish copies or derived work of any content on this website without
asking for permission, provided that you specify your source (i.e.
"sinod.katoliku.ee").

.. raw:: html

  <p xmlns:cc="http://creativecommons.org/ns#" ><a rel="cc:attributionURL"
  href="https://sinod.katoliku.ee">This work</a> by <a rel="cc:attributionURL
  dct:creator" property="cc:attributionName"
  href="https://www.katoliku.ee">Apostellik Administratuur Eestis</a> is
  licensed under <a
  href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1"
  target="_blank" rel="license noopener noreferrer"
  style="display:inline-block;">CC BY 4.0<img
  style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
  src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
  style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
  src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>
