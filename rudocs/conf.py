# -*- coding: utf-8 -*-
from pathlib import Path
docs = Path('../docs').resolve()
fn = docs / 'conf.py'
with open(fn, "rb") as fd:
    exec(compile(fd.read(), fn, 'exec'))

# html_static_path = [str(docs / '.static')]
# templates_path = [str(docs / '.templates')]

language = "ru"

html_title = "Synod on Synodality Estonia"
