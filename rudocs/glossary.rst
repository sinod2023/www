========
Glossary
========

.. glossary::

  Synod

    The **Synod on Synodality**, or simply "Synod" is the short name for the
    XVI. Ordinary General Assembly of the :term:`Synod of Bishops`, to be held
    in Rome in October 2023. The official theme of this synod is “For a synodal
    Church – Commu­nion, Participation and Mission”.

    List of previous assemblies see `Wikipedia
    <https://en.wikipedia.org/wiki/Synod_of_Bishops_in_the_Catholic_Church>`__

    Official website: https://www.synod.va/en.html

  Synod of Bishops

    An advisory body of the Catholic Church that renders cooperative assistance
    to the Pope in exercising his office. Assemblies are held every 2 to 3
    years. This body was created as part of the Vatican Council II, and as such
    is a relatively new concept. (`vatican.va
    <https://www.vatican.va/roman_curia/synod/index.htm>`__)

  Synodality

    A neologism coined by the Catholic Church to describe its *modus vivendi et
    operando*, its  way of living together, sustainably and united in diversity.

    See :ref:`neologism`.


  Sensus fidei

    A universal consent in matters of faith and morals, manifested by the whole
    people of God, from the bishops to the last of the faithful.

    https://en.wikipedia.org/wiki/Sensus_fidelium

  ITC

    International Theological Commission

    https://en.wikipedia.org/wiki/International_Theological_Commission

  CDF

    Congregation for the Doctrine of the Faith

    https://en.wikipedia.org/wiki/Congregation_for_the_Doctrine_of_the_Faith
